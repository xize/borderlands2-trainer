﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Updater
{
    class FetchUpdate
    {

        public FetchUpdate()
        {

        }

        public String DownloadUrls(String currentver, Progressbar b)
        {
            Thread t = new Thread(() =>
            {
                String versionpage = "https://gitlab.com/xize/borderlands2-trainer/-/tags/v{0}.{1}.{2}.{3}";

                int number = -1;

                while(true)
                {
                    String[] octals = currentver.Split('.');
                    if(number == -1)
                    {
                        number = int.Parse(octals[2]);
                    }

                    String newurl = String.Format(versionpage, octals[0], octals[1], number++, octals[3]);

                    b.Work("checking download: ");

                    WebClient cl = new WebClient();

                    try
                    {
                        String s = cl.DownloadString(newurl);
                    } catch(WebException e)
                    {
                        //if(e.Response.ContentType.Contains("443"))
                       // {
                            number--;
                        b.Work("checking download: ");
                        newurl = String.Format(versionpage, octals[0], octals[1], number, octals[3]);
                            MessageBox.Show(newurl);
                            String content = cl.DownloadString(newurl);
                        b.Work("checking download: ");
                        Regex r = new Regex("href=\\\"([^\\\"]", RegexOptions.IgnoreCase);
                            MatchCollection mcol = r.Matches(content);
                            foreach (Match m in mcol)
                            {
                                if (m.Success)
                                {
                                    b.Work("checking download: ");
                                    MessageBox.Show(m.Value);
                                }
                            }

                            break;
                        //}
                    }
                }
                
            });

            t.Start();

            return "";
        }

    }
}
