﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Updater
{
    class Progressbar
    {

        private int count = 0;
        private int delay = 0;

        private String[] ar =
        {
           "=>", "==>", "===>", "====>", "=====>", "======>", "=======>", "========>", "=========>", "==========>", "===========>", "============>", "=============>", "==============>"
        };

        public Progressbar()
        {

        }

        public int Delay
        {
            get
            {
                return delay;
            }
            set
            {
                this.delay = value;
            }
        }

        public bool Work(String text)
        {
            count++;

            if(count < this.ar.Length)
            {
                Thread.Sleep(this.delay);

                String fullmsg = text + "[" + ar[count] + "]";
                Console.Write(fullmsg);

                Console.SetCursorPosition(Console.CursorLeft - fullmsg.Length, Console.CursorTop);

            }

            return false;

        }

    }
}
