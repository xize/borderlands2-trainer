﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Updater
{
    static class Program
    {

        private static String ascii = @" _  _  ____  ____  ____       ___  _   _  ____    __   ____  ___ 
( \/ )(_  _)(_   )( ___)___  / __)( )_( )( ___)  /__\ (_  _)/ __)
 )  (  _)(_  / /_  )__)(___)( (__  ) _ (  )__)  /(__)\  )(  \__ \
(_/\_)(____)(____)(____)     \___)(_) (_)(____)(__)(__)(__) (___/";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            Console.Title = "..:: Trainer updater ::..";
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(ascii);
            Console.Write("\n\n");
            Console.ResetColor();
            if (args.Length == 1)
            {
                String version = args[0];

                FetchUpdate f = new FetchUpdate();
                Progressbar b = new Progressbar();
                b.Delay = 100;
                f.DownloadUrls(version, b);

            } else
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("command unknown!");
            }
            Console.ReadKey();
        }
    }
}
