﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Borderlands3Trainer
{
    public enum CheatType
    {

        GODMODE,
        SHIELD,
        GRENADES,
        NORECOIL,
        TELEPORT,
        MONEY,
        ERIDIUM,
        SERAPH,
        TORGUE,
        BADASS_TOKEN,
        GOLDEN_KEY

    }
}
