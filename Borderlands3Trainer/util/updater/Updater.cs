﻿using gitlabupdater.networking;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace gitlabupdater
{
    public partial class Updater
    {

        private String repo = "https://gitlab.com/";
        private String cleanrepo;
        private String currentversion;
        private OctalCalculator calc = new OctalCalculator();

        private String latesturl;
        private String downloadlink;
        private String downloadedcchecksum;
        private String newversion;
        private String[] changelog;

        public Updater(String repo, String currentversion)
        {
            this.repo = this.repo + repo;
            this.cleanrepo = repo;
            this.currentversion = currentversion;
        }

        public bool Verify()
        {
            FindReleaseSocket rs = new FindReleaseSocket(this);

            if(rs.SearchForRelease())
            {
                DownloadSocket dsocket = new DownloadSocket(this);
                return dsocket.FetchDownloadUrls();
            }

            return false;
        }

        public void Download(BackgroundWorker w)
        {
            w.ReportProgress(10);
            String name = "update-{" + Guid.NewGuid().ToString().ToUpper() + "}.zip";
            SaveFileDialog s = new SaveFileDialog();
            bool isOK = false;

            //fix STA issues on async threads
            Thread dialogthread = new Thread(() =>
            {
                s.FileName = name;
                s.Title = "save update...";
                s.DefaultExt = "zip";
                s.Filter = "*.zip|zip archive";
                s.OverwritePrompt = true;
                DialogResult r = s.ShowDialog();
                if (r == DialogResult.OK)
                {
                    isOK = !isOK;
                }
            });

            dialogthread.SetApartmentState(ApartmentState.STA);
            dialogthread.Start();
            dialogthread.Join();

            if (isOK)
            {
                String downloadpath = s.FileName;

                using (WebClient c = new WebClient())
                {
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                    w.ReportProgress(40);

                    c.DownloadFile(this.DownloadString, downloadpath);

                    w.ReportProgress(50);

                    if (this.DownloadedCheckSum == null || this.DownloadedCheckSum == "")
                    {
                        MessageBox.Show("no checksum files found!\nplease note this file can't be checked against drive by malware infections!", "warning!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        ProcessStartInfo info = new ProcessStartInfo();
                        Process p = Process.Start("explorer.exe", downloadpath);
                    }
                    else
                    {
                        w.ReportProgress(60);

                        String checksum = c.DownloadString(this.DownloadedCheckSum);

                        byte[] fbytes = File.ReadAllBytes(downloadpath);
                        byte[] computedbytes = SHA256.Create().ComputeHash(fbytes);
                        StringBuilder build = new StringBuilder();
                        foreach (byte b in computedbytes)
                        {
                            build.Append(b.ToString("X2"));
                        }

                        if (build.ToString() == checksum)
                        {
                            w.ReportProgress(80);
                            Process.Start("explorer.exe", downloadpath);
                        }
                        else
                        {
                            MessageBox.Show("the following checksums are not matching!\nFile: " + build.ToString() + "\nexpected: " + checksum, "checksum mismatch!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            w.ReportProgress(100);
        }

        public void Reset()
        {
            this.DownloadString = "";
            this.DownloadedCheckSum = "";
            this.LatestURL = "";
            this.NewVersion = "";
        }

    }
}
