﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace gitlabupdater
{
    static class Test
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {

            Updater update = new Updater("xize/borderlands2-trainer", "1.9.0.4");

            Console.WriteLine("searching for updates...");
            if(update.Verify())
            {
                Console.WriteLine("update found last version: "+update.NewVersion+"!");
                Console.WriteLine("downloading file...");

                if(!Directory.Exists("C:/download"))
                {
                    Directory.CreateDirectory("C:/download");
                }

                update.Download(null);
            } else
            {
                Console.WriteLine("failed to find updates!");
            }
        }
    }
}
