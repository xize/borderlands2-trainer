﻿using Borderlands3Trainer.cheats;
using Borderlands3Trainer.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    public partial class ScanAOB : Form
    {

        private IPlayer p;
        private Window w;

        public ScanAOB(Window w, IPlayer p)
        {
            InitializeComponent();
            this.p = p;
            this.w = w;
        }

        private void ScanAOB_Load(object sender, EventArgs e)
        {
            this.worker.RunWorkerAsync();
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            PlayerModel m = (PlayerModel)p;
            m.PerformAOBScan(this.worker);
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 20)
            {
                //found player
                this.playerstatus.Text = "found";
                this.playerstatus.ForeColor = Color.Green;
                Console.Beep();
                //Speech.Speak("found player signature found");
            } else if (e.ProgressPercentage == 25)
            {
                //not found player
                Console.Beep(100, 800);
                //Speech.Speak("player signature base not found!");

            } else if (e.ProgressPercentage == 50)
            {
                //found instakill
                this.instakillstatus.Text = "found";
                this.instakillstatus.ForeColor = Color.Green;
                Console.Beep();
                //Speech.Speak("found instakill signature");
            } else if(e.ProgressPercentage == 55) {
                //not found instakill signature
                Console.Beep(100, 800);
                //Speech.Speak("instakkill signature base not found!");
            } else if(e.ProgressPercentage == 80)
            {
                //found golden keys
                this.goldenkeystatus.Text = "found";
                this.goldenkeystatus.ForeColor = Color.Green;
                Console.Beep();
                //Speech.Speak("found golden key signature");
            } else if(e.ProgressPercentage == 85)
            {
                //not found golden key signature
                Console.Beep(100, 800);
                //Speech.Speak("golden key signature base not found!");
            }

            this.progresslabel.Text = "progress: "+e.ProgressPercentage+"%";
            this.progressbar.Value = e.ProgressPercentage;

            if(this.worker.CancellationPending)
            {
                //this.Close();
            }
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!this.worker.CancellationPending)
            {
                w.aobstatus.ForeColor = Color.Green;
                w.aobstatus.Text = "all found!";

            }
            //this.Close();
        }

        private void Cancelbtn_Click(object sender, EventArgs e)
        {
                this.cancelbtn.Enabled = false;
                this.worker.CancelAsync();
        }
    }
}
