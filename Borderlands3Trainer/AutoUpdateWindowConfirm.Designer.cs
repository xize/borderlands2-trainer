﻿namespace Borderlands3Trainer
{
    partial class AutoUpdateWindowConfirm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoUpdateWindowConfirm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.oldversion = new System.Windows.Forms.Label();
            this.newversion = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.changelog = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.processlabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Update Confirmination";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(206, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "X";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.LimeGreen;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(171, 74);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(49, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Download";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(5, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "current version:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(5, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "newer version:";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.DarkGreen;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(127, 74);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(38, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // oldversion
            // 
            this.oldversion.AutoSize = true;
            this.oldversion.BackColor = System.Drawing.Color.Transparent;
            this.oldversion.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.oldversion.ForeColor = System.Drawing.Color.Gray;
            this.oldversion.Location = new System.Drawing.Point(90, 43);
            this.oldversion.Name = "oldversion";
            this.oldversion.Size = new System.Drawing.Size(19, 13);
            this.oldversion.TabIndex = 6;
            this.oldversion.Text = "{0}";
            // 
            // newversion
            // 
            this.newversion.AutoSize = true;
            this.newversion.BackColor = System.Drawing.Color.Transparent;
            this.newversion.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.newversion.ForeColor = System.Drawing.Color.Gray;
            this.newversion.Location = new System.Drawing.Point(84, 56);
            this.newversion.Name = "newversion";
            this.newversion.Size = new System.Drawing.Size(19, 13);
            this.newversion.TabIndex = 7;
            this.newversion.Text = "{0}";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(6, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 12);
            this.label7.TabIndex = 8;
            this.label7.Text = "A new update is available";
            // 
            // progress
            // 
            this.progress.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.progress.Location = new System.Drawing.Point(8, 97);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(212, 3);
            this.progress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progress.TabIndex = 9;
            // 
            // changelog
            // 
            this.changelog.BackColor = System.Drawing.Color.Black;
            this.changelog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.changelog.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.changelog.ForeColor = System.Drawing.Color.Gray;
            this.changelog.Location = new System.Drawing.Point(8, 106);
            this.changelog.Name = "changelog";
            this.changelog.Size = new System.Drawing.Size(212, 51);
            this.changelog.TabIndex = 10;
            this.changelog.Text = "";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Underline);
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(12, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "update notes";
            this.label5.Click += new System.EventHandler(this.Label5_Click);
            // 
            // processlabel
            // 
            this.processlabel.AutoSize = true;
            this.processlabel.BackColor = System.Drawing.Color.Transparent;
            this.processlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.processlabel.ForeColor = System.Drawing.Color.Gray;
            this.processlabel.Location = new System.Drawing.Point(5, 78);
            this.processlabel.Name = "processlabel";
            this.processlabel.Size = new System.Drawing.Size(46, 13);
            this.processlabel.TabIndex = 12;
            this.processlabel.Text = "process:";
            // 
            // AutoUpdateWindowConfirm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Borderlands3Trainer.Properties.Resources.center2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(232, 109);
            this.Controls.Add(this.processlabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.changelog);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.newversion);
            this.Controls.Add(this.oldversion);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AutoUpdateWindowConfirm";
            this.Text = "Update Confirmination";
            this.Load += new System.EventHandler(this.AutoUpdateWindowConfirm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label oldversion;
        private System.Windows.Forms.Label newversion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.RichTextBox changelog;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label processlabel;
    }
}