﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.cheats
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    class Cheat : Attribute
    {

        private String cheat;

        public Cheat(String cheatname)
        {
            this.cheat = cheatname;
        }

        public String GetCheat
        {
            get {
                return this.cheat;
            }
        }

    }
}
