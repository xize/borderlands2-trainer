﻿using Borderlands3Trainer.util;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer.cheats
{
    public class PlayerModel : ModelCheat, IPlayer, IGoldenKey
    {

        private Location lastLocation;

        String aob = ":D";

        public PlayerModel(Mem m) : base("Borderlands3.exe+0x0608FEC8", m){}


        public void FreezeHealth()
        {
            String offsets = "0x0,0x80,0x10,0x10,0x638,0x180,0x198";
            float currenthealth = this.GetMemory().readFloat(this.GetBaseAddress()+","+offsets, "", false);
            this.GetMemory().FreezeValue(this.GetBaseAddress()+","+offsets, "float", currenthealth+"");
        }

        public void UnFreezeHealth()
        {
            String offsets = "0x0,0x8,0x10,0x10,0x638,0x180,0x198";
            this.GetMemory().UnfreezeValue(this.GetBaseAddress() + "," + offsets);
        }

        public void FreezeShield()
        {
            String offsets = "0x0,0x8,0x10,0x18,0x5D8,0x180,0x290";
            float currentshield = this.GetMemory().readFloat(this.GetBaseAddress()+","+offsets, "", false);
            this.GetMemory().FreezeValue(this.GetBaseAddress()+","+offsets, "float", currentshield+"");
        }

        public void UnFreezeShield()
        {
            String offsets = "0x0,0x8,0x10,0x18,0x5D8,0x180,0x290";
            this.GetMemory().UnfreezeValue(this.GetBaseAddress()+","+offsets);
        }

        public void FreezeGrenades()
        {
            String offsets = "0x0,0x8,0x10,0x18,0x5D8,0x180,0x9C";
            double doubleval = this.GetMemory().readDouble(this.GetBaseAddress()+","+offsets, "", false);
            this.GetMemory().FreezeValue(this.GetBaseAddress()+","+offsets, "double", doubleval+"");
        }

        public void UnFreezeGrenades()
        {
            String offsets = "0x0,0x8,0x10,0x18,0x5D8,0x180,0x9C";
            this.GetMemory().UnfreezeValue(this.GetBaseAddress()+","+offsets);
        }

        public void FreezeAmmo()
        {

            //in total we need to use pointers for the weapon catagory: pistol, SMG, MG, RPG, Sniper, Shotgun
            //6 pointers which are all use the double type.

            //Pistol
            String poffsets = "0x0,0x8,0x10,0x10,0x638,0x180,0x384";
            double currentpistolammo = this.GetMemory().readDouble(this.GetBaseAddress()+","+poffsets, "", false);
            this.GetMemory().FreezeValue(this.GetBaseAddress()+","+poffsets, "double",currentpistolammo+"");

            //SMG
            String smoffsets = "0x0,0x8,0x10,0x18,0x5D8,0x180,0x574";
            double currentsmg = this.GetMemory().readDouble(this.GetBaseAddress() + "," + smoffsets, "", false);
            this.GetMemory().FreezeValue(this.GetBaseAddress() + "," + smoffsets, "double", currentsmg+"");

            //MG
            String moffsets = "0x68,0x178,0x88,0x70,0x8,0xF0,0x150,0x66C";
            double currentmg = this.GetMemory().readDouble(this.GetBaseAddress() + "," + moffsets, "", false);
            this.GetMemory().FreezeValue(this.GetBaseAddress() + "," + moffsets, "double", currentmg + "");

            //Shotgun
            String shoffsets = "0x78,0x5A8,0x20,0x250,0x980,0x4F0,0x440,0x47C";
            double currentshotgun = this.GetMemory().readDouble(this.GetBaseAddress()+","+shoffsets, "", false);
            this.GetMemory().FreezeValue(this.GetBaseAddress() + "," + shoffsets, "double", currentshotgun + "");

            //Sniper
            String snoffsets = "0x0,0x8,0x10,0x18,0x5D8,0x180,0x764";
            double currentsniper = this.GetMemory().readDouble(this.GetBaseAddress() + "," + snoffsets, "", false);
            this.GetMemory().FreezeValue(this.GetBaseAddress() + "," + snoffsets, "double", currentsniper+"");

            //RPG
            String roffsets = "0x210,0x8,0x558,0x550,0x68,0x180,0x85C";
            double currentrockets = this.GetMemory().readDouble(this.GetBaseAddress()+","+roffsets, "", false);
            this.GetMemory().FreezeValue(this.GetBaseAddress()+","+roffsets, "double", currentrockets+"");
        }

        public void UnFreezeAmmo()
        {
            //in total we need to use pointers for the weapon catagory: pistol, SMG, MG, RPG, Sniper, Shotgun
            //6 pointers which are all use the double type.

            //Pistol
            String poffsets = "0x0,0x8,0x10,0x10,0x638,0x180,0x384";
            this.GetMemory().UnfreezeValue(this.GetBaseAddress() + "," + poffsets);

            //SMG
            String smoffsets = "0x0,0x8,0x10,0x18,0x5D8,0x180,0x574";
            this.GetMemory().UnfreezeValue(this.GetBaseAddress() + "," + smoffsets);

            //MG
            String moffsets = "0x68,0x178,0x88,0x70,0x8,0xF0,0x150,0x66C";
          
            this.GetMemory().UnfreezeValue(this.GetBaseAddress() + "," + moffsets);

            //Shotgun
            String shoffsets = "0x78,0x5A8,0x20,0x250,0x980,0x4F0,0x440,0x47C";
            this.GetMemory().UnfreezeValue(this.GetBaseAddress() + "," + shoffsets);

            //Sniper
            String snoffsets = "0x0,0x8,0x10,0x18,0x5D8,0x180,0x764";
            this.GetMemory().UnfreezeValue(this.GetBaseAddress() + "," + snoffsets);

            //RPG
            String roffsets = "0x210,0x8,0x558,0x550,0x68,0x180,0x85C";
            this.GetMemory().UnfreezeValue(this.GetBaseAddress() + "," + roffsets);
        }

        public Location GetCurrentLocation()
        {
            String xoffset = "0x0,0x194,0x28,0x2A8,0xC4,0xA4,0x60";
            String yoffset = "0x0,0x28,0x3C,0x11C,0x18C,0xDC,0x68";
            String zoffset = "0x0,0x100,0x28,0x2A8,0x108,0x1EC,0x64";

            float xcoord = this.GetMemory().readFloat(this.GetBaseAddress() + "," + xoffset);
            float ycoord = this.GetMemory().readFloat(this.GetBaseAddress() + "," + yoffset);
            float zcoord = this.GetMemory().readFloat(this.GetBaseAddress() + "," + zoffset);

            return new Location(xcoord, ycoord, zcoord);
        }

        public void TeleportJump()
        {
            String yoffset = "0x0,0x28,0x3C,0x11C,0x18C,0xDC,0x68";

            float y = this.GetMemory().readFloat(this.GetBaseAddress()+","+yoffset, "", false) + 100;
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+yoffset, "float", y+"");
        }

        public void TeleportTo(TeleportType type)
        {
            String xoffset = "0x0,0x194,0x28,0x2A8,0xC4,0xA4,0x60";
            String yoffset = "0x0,0x28,0x3C,0x11C,0x18C,0xDC,0x68";
            String zoffset = "0x0,0x100,0x28,0x2A8,0x108,0x1EC,0x64";

            this.lastLocation = this.GetCurrentLocation();

            this.GetMemory().writeMemory(this.GetBaseAddress()+","+xoffset, "float", type.GetX());
            this.GetMemory().writeMemory(this.GetBaseAddress() + ","+yoffset, "float", type.GetY());
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+zoffset, "float", type.GetZ());
        }

        public void UndoTeleport()
        {

            if(this.lastLocation == null)
            {
                Console.Beep();
                return;
            }

            String xoffset = "0x0,0x194,0x28,0x2A8,0xC4,0xA4,0x60";
            String yoffset = "0x0,0x28,0x3C,0x11C,0x18C,0xDC,0x68";
            String zoffset = "0x0,0x100,0x28,0x2A8,0x108,0x1EC,0x64";

            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + xoffset, "float", this.lastLocation.GetX()+"");
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + yoffset, "float", this.lastLocation.GetY() + "");
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + zoffset, "float", this.lastLocation.GetZ() + "");
        }

        public void SetMoney(int money)
        {
            String offsets = "0x0,0x76C,0x28,0x34C,0x108,0x194,0x2A0";
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "int", money+"");
        }

        public int GetMoney()
        {
            String offsets = "0x0,0x76C,0x28,0x34C,0x108,0x194,0x2A0";
            int money = this.GetMemory().readInt(this.GetBaseAddress()+","+offsets);
            return money;
        }

        public int GetEridium()
        {
            String offsets = "0x0,0xAC,0xA4,0x28,0x3C,0x94,0x2B4";
            int n = this.GetMemory().readInt(this.GetBaseAddress()+","+offsets);
            return n;
        }

        public void SetEridium(int amount)
        {
            String offsets = "0x0,0xAC,0xA4,0x28,0x3C,0x94,0x2B4";
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "int", amount + "");
        }

        //multi level pointer, result pointer is a byte.
        public int GetGoldenKeys()
        {

            throw new ArgumentNullException("not implemented");
        }

        public void SetGoldenKeys(int amount)
        {
            throw new ArgumentNullException("not implemented");
        }

        public void Fly()
        {
            String offsets = "0x0,0x194,0xA4,0x54,0x0,0x28,0x190,0x10C";

            float jumpstate = this.GetMemory().readFloat(this.GetBaseAddress()+","+offsets);

            if(jumpstate == 0)
            {
                this.TeleportJump();
            }

            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "float", 1000 + "");
        }

        public void SetLevel(int i)
        {
            String offsets = "0x0,0x28,0x48,0x2A8,0x4C,0x1A8,0x6C";
            int clevel = this.GetLevelXP(i);

            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "float", clevel+"");

        }

        public int GetLevelXP(int wantedlevel)
        {

            wantedlevel = wantedlevel - 1;


            /*
             * from: https://borderlands.fandom.com/wiki/Experience_Points
             * In Borderlands, Borderlands 2, and Borderlands: The Pre-Sequel,
             * the amount of experience necessary to reach level x equals ceil(60x2.8 − 60).
             */

            int[] levels =
            {
                0,
                358,
                1241,
                2850,
                5376,
                8997,
                13886,
                20208,
                28126,
                37798,
                49377,
                63016,
                78861,
                97061,
                117757,
                141092,
                167206,
                196238,
                228322,
                263595,
                302190,
                344238,
                389873,
                439222,
                492414,
                549578,
                610840,
                676325,
                746158,
                820463,
                899363,
                982980,
                1071435,
                1164850,
                1263343,
                1367034,
                1476041,
                1590483,
                1710476,
                1836137,
                1967582,
                2104926,
                2248285,
                2397772,
                2553501,
                2715586,
                2884139,
                3059273,
                3241098,
                3429728,
                3625271,
                3827840,
                4037543,
                4254491,
                4478792,
                4710556,
                4949890,
                5196902,
                5451701,
                5714393,
                5985086,
                6263885,
                6550897,
                6846227,
                7149982,
                7462266,
                7783184,
                8112840,
                8451340,
                8798786,
                9155282,
                9520932,
                9895840,
                10280105,
                10673832,
                11077123,
                11490079,
                11912803,
                12345396,
                12787958
            };

            return levels[wantedlevel];

        }

        private void CatogorizeBaseSpeak(String basename, Type clazz)
        {
            MethodInfo[] m = clazz.GetMethods();

            List<String> catdata = new List<string>();

            foreach (MethodInfo method in m)
            {
                bool attribfound = method.GetCustomAttributes(typeof(Cheat), false).Any();
                if (attribfound)
                {
                    Cheat c = (Cheat)method.GetCustomAttribute(typeof(Cheat), false);
                    catdata.Add(c.GetCheat);

                }

            }
            String cheatlist = String.Join(", ", catdata);

            Speech.SpeakSync("scanning for "+ basename +" base signature containing the following cheats: ");
            Speech.SpeakSync(cheatlist);
        }

        public override void PerformAOBScan(BackgroundWorker worker)
        {
            /*
            //begin player base scan
            worker.ReportProgress(10);
            if(worker.CancellationPending)
            {
                return;
            }
            Task<IEnumerable<long>> t1 = this.GetMemory().AoBScan(this.aob_signature, true, false);
            t1.ConfigureAwait(true);
            t1.Wait();
            long iaddress = t1.Result.FirstOrDefault();
            if (iaddress > 0)
            {

                String address = Convert.ToString(iaddress, 16).ToUpper();
                if (address != "0")
                {
                    worker.ReportProgress(20);
                    this.SetBaseAddress(address);
                    worker.ReportProgress(50);
                } else
                {
                    worker.ReportProgress(25);
                }
            } else
            {
                worker.ReportProgress(25);
            }
            //end player scan

            //begin instakkill scan
            worker.ReportProgress(30);
            if (worker.CancellationPending)
            {
                return;
            }
            Task<IEnumerable<long>> t2 = this.GetMemory().AoBScan(this.attack_signature, true, false);
            t2.ConfigureAwait(true);
            t2.Wait();
            long iaddress2 = t2.Result.FirstOrDefault();
            if (iaddress2 > 0)
            {

                String address2 = Convert.ToString(iaddress2, 16).ToUpper();
                if (address2 != "0")
                {
                    this.attackbase = address2;
                    worker.ReportProgress(50);
                } else
                {
                    worker.ReportProgress(55);
                }
            } else
            {
                worker.ReportProgress(55);
            }

            worker.ReportProgress(60);
            if (worker.CancellationPending)
            {
                return;
            }
            Task<IEnumerable<long>> t3 = this.GetMemory().AoBScan(this.goldenkey_signature, true, false);
            t3.ConfigureAwait(true);
            t3.Wait();
            long iaddress3 = t3.Result.FirstOrDefault();
            if (iaddress3 > 0)
            {

                String address3 = Convert.ToString(iaddress3, 16).ToUpper();
                if (address3 != "0")
                {
                    this.goldenkey_base = address3;
                    worker.ReportProgress(80);
                } else
                {
                    worker.ReportProgress(85);
                }
            } else
            {
                worker.ReportProgress(85);
            }

            //end golden key scan
            if (worker.CancellationPending)
            {
                return;
            }
            worker.ReportProgress(100);
            Thread.Sleep(2000);
            */
        }

    }
}
