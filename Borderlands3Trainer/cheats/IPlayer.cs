﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.cheats
{
    public interface IPlayer
    {

        [Cheat("Godmode")]
        void FreezeHealth();

        void UnFreezeHealth();

        [Cheat("infinitive shield")]
        void FreezeShield();

        void UnFreezeShield();

        [Cheat("grenades")]
        void FreezeGrenades();

        void UnFreezeGrenades();

        void FreezeAmmo();

        void UnFreezeAmmo();

        Location GetCurrentLocation();

        void TeleportJump();

        [Cheat("teleport")]
        void TeleportTo(TeleportType type);

        void UndoTeleport();

        [Cheat("level")]
        void SetLevel(int num);

        int GetMoney();

        [Cheat("money")]
        void SetMoney(int money);

        int GetEridium();

        [Cheat("erdium")]
        void SetEridium(int amount);

        [Cheat("fly")]
        void Fly();

    }
}
