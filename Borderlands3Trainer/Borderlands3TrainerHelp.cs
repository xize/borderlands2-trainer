﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    public partial class Borderlands3TrainerHelp : Form
    {

        public Borderlands3TrainerHelp()
        {
            InitializeComponent();
            this.label4.Text = String.Format(label4.Text, Application.ProductVersion);
        }

        private void Label3_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
