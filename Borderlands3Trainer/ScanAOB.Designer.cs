﻿namespace Borderlands3Trainer
{
    partial class ScanAOB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScanAOB));
            this.groupbox = new System.Windows.Forms.GroupBox();
            this.progresslabel = new System.Windows.Forms.Label();
            this.goldenkeystatus = new System.Windows.Forms.Label();
            this.instakillstatus = new System.Windows.Forms.Label();
            this.playerstatus = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.progressbar = new System.Windows.Forms.ProgressBar();
            this.cancelbtn = new System.Windows.Forms.Button();
            this.worker = new System.ComponentModel.BackgroundWorker();
            this.groupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupbox
            // 
            this.groupbox.Controls.Add(this.progresslabel);
            this.groupbox.Controls.Add(this.goldenkeystatus);
            this.groupbox.Controls.Add(this.instakillstatus);
            this.groupbox.Controls.Add(this.playerstatus);
            this.groupbox.Controls.Add(this.label3);
            this.groupbox.Controls.Add(this.label2);
            this.groupbox.Controls.Add(this.label1);
            this.groupbox.Location = new System.Drawing.Point(12, 12);
            this.groupbox.Name = "groupbox";
            this.groupbox.Size = new System.Drawing.Size(238, 81);
            this.groupbox.TabIndex = 0;
            this.groupbox.TabStop = false;
            this.groupbox.Text = "signature status:";
            // 
            // progresslabel
            // 
            this.progresslabel.AutoSize = true;
            this.progresslabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progresslabel.Location = new System.Drawing.Point(3, 65);
            this.progresslabel.Name = "progresslabel";
            this.progresslabel.Size = new System.Drawing.Size(67, 13);
            this.progresslabel.TabIndex = 6;
            this.progresslabel.Text = "progress: 0%";
            // 
            // goldenkeystatus
            // 
            this.goldenkeystatus.AutoSize = true;
            this.goldenkeystatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goldenkeystatus.ForeColor = System.Drawing.Color.Red;
            this.goldenkeystatus.Location = new System.Drawing.Point(180, 43);
            this.goldenkeystatus.Name = "goldenkeystatus";
            this.goldenkeystatus.Size = new System.Drawing.Size(52, 13);
            this.goldenkeystatus.TabIndex = 5;
            this.goldenkeystatus.Text = "not found";
            // 
            // instakillstatus
            // 
            this.instakillstatus.AutoSize = true;
            this.instakillstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instakillstatus.ForeColor = System.Drawing.Color.Red;
            this.instakillstatus.Location = new System.Drawing.Point(180, 30);
            this.instakillstatus.Name = "instakillstatus";
            this.instakillstatus.Size = new System.Drawing.Size(52, 13);
            this.instakillstatus.TabIndex = 4;
            this.instakillstatus.Text = "not found";
            // 
            // playerstatus
            // 
            this.playerstatus.AutoSize = true;
            this.playerstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerstatus.ForeColor = System.Drawing.Color.Red;
            this.playerstatus.Location = new System.Drawing.Point(180, 16);
            this.playerstatus.Name = "playerstatus";
            this.playerstatus.Size = new System.Drawing.Size(52, 13);
            this.playerstatus.TabIndex = 3;
            this.playerstatus.Text = "not found";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "- golden key signature:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "- instakill signature:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "- player base signature:";
            // 
            // progressbar
            // 
            this.progressbar.Location = new System.Drawing.Point(12, 99);
            this.progressbar.Name = "progressbar";
            this.progressbar.Size = new System.Drawing.Size(238, 23);
            this.progressbar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressbar.TabIndex = 1;
            // 
            // cancelbtn
            // 
            this.cancelbtn.Location = new System.Drawing.Point(92, 128);
            this.cancelbtn.Name = "cancelbtn";
            this.cancelbtn.Size = new System.Drawing.Size(75, 23);
            this.cancelbtn.TabIndex = 2;
            this.cancelbtn.Text = "&Cancel";
            this.cancelbtn.UseVisualStyleBackColor = true;
            this.cancelbtn.Click += new System.EventHandler(this.Cancelbtn_Click);
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Worker_DoWork);
            this.worker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Worker_ProgressChanged);
            this.worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Worker_RunWorkerCompleted);
            // 
            // ScanAOB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 158);
            this.Controls.Add(this.cancelbtn);
            this.Controls.Add(this.progressbar);
            this.Controls.Add(this.groupbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScanAOB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "scanning signature *";
            this.Load += new System.EventHandler(this.ScanAOB_Load);
            this.groupbox.ResumeLayout(false);
            this.groupbox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label goldenkeystatus;
        private System.Windows.Forms.Label instakillstatus;
        private System.Windows.Forms.Label playerstatus;
        private System.Windows.Forms.ProgressBar progressbar;
        private System.Windows.Forms.Button cancelbtn;
        private System.Windows.Forms.Label progresslabel;
        private System.ComponentModel.BackgroundWorker worker;
    }
}