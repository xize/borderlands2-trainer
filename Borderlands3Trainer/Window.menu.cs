﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    partial class Window
    {

        private void menucheat(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;

            if (item.Text.Contains("godmode"))
            {
                this.godmodecheckbox.Checked = item.Checked;
            }
            else if (item.Text.Contains("shield"))
            {
                this.shieldcheckbox.Checked = item.Checked;
            }
            else if (item.Text.Contains("grenade"))
            {
                this.grandecheckbox.Checked = item.Checked;
            }
            else if (item.Text.Contains("recoil"))
            {
                this.noRecoilToolStripMenuItem.Checked = item.Checked;
            }
            else if (item.Text.Contains("money"))
            {
                this.moneybtn.PerformClick();
            }
            else if (item.Text.Contains("Eridium"))
            {
                this.eridiumbtn.PerformClick();
            }
            else if (item.Text.Contains("golden"))
            {
                this.goldkeybtn.PerformClick();
            }
            else if (item.Text.Contains("fly"))
            {
                this.flycheckbox.Checked = !this.flycheckbox.Checked;
            }
        }

        private void GodmodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.godmodecheckbox.Checked = !this.godmodecheckbox.Checked;
        }

        private void ShieldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.shieldcheckbox.Checked = !this.shieldcheckbox.Checked;
        }


        private void InfinitiveGrenadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.grandecheckbox.Checked = !this.grandecheckbox.Checked;
        }

        private void GiveMoneyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.moneybtn.PerformClick();
        }

        private void GiveEridiumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.eridiumbtn.PerformClick();
        }

        private void GiveGoldenKeysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.goldkeybtn.PerformClick();
        }

        private void LevelUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.levelbtn.PerformClick();
        }

        private void FlyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.flycheckbox.Checked = !this.flycheckbox.Checked;
        }

    }
}
