﻿using Borderlands2Trainer.cheats;
using Borderlands2Trainer.util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    partial class Window
    {


        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        private HotKeyProvider hotkey;

        public void RegisterHotKeys()
        {
            this.hotkey = new HotKeyProvider(this);
            hotkey.RegisterKey(Keys.NumPad0);
            hotkey.RegisterKey(Keys.NumPad1);
            hotkey.RegisterKey(Keys.NumPad2);
            //hotkey.RegisterKey(Keys.NumPad3);
            hotkey.RegisterKey(Keys.NumPad4);
            hotkey.RegisterKey(Keys.NumPad5);
            hotkey.RegisterKey(Keys.NumPad6);
            hotkey.RegisterKey(Keys.NumPad7);
            hotkey.RegisterKey(Keys.NumPad8);
            hotkey.RegisterKey(Keys.NumPad9);
            hotkey.RegisterKey(Keys.Add);
            hotkey.RegisterKey(Keys.Left);
            hotkey.RegisterKey(Keys.Down);
            hotkey.RegisterKey(Keys.Right);
            hotkey.RegisterKey(Keys.Multiply);
            hotkey.RegisterKey(Keys.F3);
            hotkey.RegisterKey(Keys.F4);
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == 0x0312)
            {
                /* Note that the three lines below are not needed if you only want to register one hotkey.
                 * The below lines are useful in case you want to register multiple keys, which you can use a switch with the id as argument, or if you want to know which key/modifier was pressed for some particular reason. */


                int id = m.WParam.ToInt32();

                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);                  // The key of the hotkey that was pressed.

                if (this.m.theProc == null)
                {
                    Speech.Speak("Error!, the process was not found!");
                    return;
                }

                //use GetWindowThreadProcessId to check wether the focus is on the trainer or the game and not a completely different window.
                int processout;
                GetWindowThreadProcessId(GetForegroundWindow(), out processout);

                if (this.m.theProc.Id == processout || Process.GetCurrentProcess().Id == processout)
                {
                    if (key == Keys.NumPad0)
                    {
                        if (this.godmodecheckbox.Checked)
                        {
                            this.godmodecheckbox.Checked = false;
                            Speech.Speak(this.mute, "godmode disabled");
                        }
                        else
                        {
                            this.godmodecheckbox.Checked = true;
                            Speech.Speak(this.mute, "godmode enabled");
                        }
                    }
                    else if (key == Keys.NumPad1)
                    {
                        if (this.shieldcheckbox.Checked)
                        {
                            this.shieldcheckbox.Checked = false;
                            Speech.Speak(this.mute, "infinitive shield disabled");
                        }
                        else
                        {
                            this.shieldcheckbox.Checked = true;
                            Speech.Speak(this.mute, "infinitive shield enabled");
                        }
                    }
                    else if (key == Keys.NumPad2)
                    {
                        if (this.grandecheckbox.Checked)
                        {
                            this.grandecheckbox.Checked = false;
                            Speech.Speak(this.mute, "infinitive grenades disabled");
                        }
                        else
                        {
                            this.grandecheckbox.Checked = true;
                            Speech.Speak(this.mute, "infinitive grenades enabled");
                        }
                    }
                    else if (key == Keys.NumPad3)
                    {
                        if (this.norecoilcheckbox.Checked)
                        {
                            this.norecoilcheckbox.Checked = false;
                            Speech.Speak(this.mute, "no recoil disabled");
                        }
                        else
                        {
                            this.norecoilcheckbox.Checked = true;
                            Speech.Speak(this.mute, "no recoil enabled");
                        }
                    }
                    else if (key == Keys.NumPad4)
                    {
                        this.DoTeleport();
                    }
                    else if (key == Keys.Right)
                    {
                        int index = this.teleportbox.SelectedIndex;

                        if ((index + 1) <= this.teleportbox.Items.Count - 1)
                        {
                            this.teleportbox.SelectedIndex = index + 1;
                            this.teleportbox.Text = (String)this.teleportbox.SelectedItem;
                            TeleportType type = TeleportType.ValueOf((String)this.teleportbox.SelectedItem);
                            Speech.Speak("switching next to option: " + type.getVoiceName());
                        }
                        else
                        {
                            Console.Beep();
                        }
                    }
                    else if (key == Keys.Left)
                    {
                        int index = this.teleportbox.SelectedIndex;

                        if ((index - 1) >= 0)
                        {
                            this.teleportbox.SelectedIndex = index - 1;
                            this.teleportbox.Text = (String)this.teleportbox.SelectedItem;
                            TeleportType type = TeleportType.ValueOf((String)this.teleportbox.SelectedItem);
                            Speech.Speak("switching back to option: " + type.getVoiceName());
                        }
                        else
                        {
                            Console.Beep();
                        }
                    }
                    else if (key == Keys.Down)
                    {
                        this.tpundo.PerformClick();
                    }
                    else if (key == Keys.NumPad5)
                    {
                        this.moneybtn.PerformClick();
                    }
                    else if (key == Keys.NumPad6)
                    {
                        this.eridiumbtn.PerformClick();
                    }
                    else if (key == Keys.NumPad7)
                    {
                        this.goldkeybtn.PerformClick();
                    }
                    else if (key == Keys.NumPad8)
                    {
                        this.seraphbtn.PerformClick();
                    }
                    else if (key == Keys.NumPad9)
                    {
                        this.torguebtn.PerformClick();
                    }
                    else if (key == Keys.Add)
                    {
                        if (this.badassgeneratorcheckbox.Checked)
                        {
                            this.badassgeneratorcheckbox.Checked = false;
                            Speech.Speak(this.mute, "disabling bad ass token generator");
                        }
                        else
                        {
                            this.badassgeneratorcheckbox.Checked = true;
                            Speech.Speak(this.mute, "enabling bad ass token generator");
                        }
                    }
                    else if (key == Keys.Multiply)
                    {
                        this.levelbtn.PerformClick();
                        Console.Beep();
                    }
                    else if (key == Keys.F3)
                    {
                        if (this.flycheckbox.Checked)
                        {
                            this.flycheckbox.Checked = false;
                            Speech.Speak(this.mute, "disabling fly");
                        }
                        else
                        {
                            this.flycheckbox.Checked = true;
                            Speech.Speak(this.mute, "enabling fly");
                        }
                    }
                    else if (key == Keys.Space)
                    {
                        this.GetTrainer().GetPlayer().Fly();
                    }
                    else if (key == Keys.F4)
                    {

                        this.instakillbtn.PerformClick();
                    }
                }
            }
        }

        public void UnRegisterHotKeys()
        {
            this.hotkey.UnRegisterAll();
        }

        private void Disablehotkeyscheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.disablehotkeyscheckbox.Checked)
            {
                this.UnRegisterHotKeys();
            }
            else
            {
                this.RegisterHotKeys();
            }
        }

        private void Mute_CheckedChanged(object sender, EventArgs e)
        {
            if (this.mute.Checked)
            {
                this.mute.ForeColor = Color.Green;
            }
            else
            {
                this.mute.ForeColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            }
        }

    }
}
