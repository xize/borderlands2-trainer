﻿using Borderlands2Trainer.cheats;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    public partial class TeleportManager : Form
    {

        private Window win;
        private bool isMouseClicked = false;
        private Point lastLocation;
        private Trainer trainer;
        private BackgroundWorker worker;
        private String datafolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)+@"/0c3";
        private Cursor grab;
        private Cursor ungrab;


        private HashSet<CustomTeleportType> types = new HashSet<CustomTeleportType>();

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public TeleportManager(Window win, Trainer trainer)
        {
            this.win = win;
            this.trainer = trainer;
            InitializeComponent();

            this.teleportlist.Items.Clear();

            try
            {
                this.types = this.GetTeleportTypesFromFile().ToHashSet();
                foreach (CustomTeleportType type in types)
                {
                    this.teleportlist.Items.Add(type.GetName());
                }
            }
            catch (Exception)
            {
                //do nothing :)
            }
        }


        private void TeleportManager_Load(object sender, EventArgs e)
        {

            this.ungrab = new Cursor(Properties.Resources.grabicon1.GetHicon());
            this.grab = new Cursor(Properties.Resources.grabicon2.GetHicon());
            this.Cursor = this.ungrab;

            this.makeMoveAble(this);

            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));

            this.teleportlist.EditChanged += new EventHandler(onItemRenamed);

            if(!Directory.Exists(datafolder))
            {
                Directory.CreateDirectory(datafolder);
            }

            if(!Directory.Exists(datafolder+"/borderlands2trainer"))
            {
                Directory.CreateDirectory(datafolder + "/borderlands2trainer");
            }

            this.worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(backgroundworkerevent);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(workercomplete);
            worker.RunWorkerAsync();
           
        }

        private void onItemRenamed(object sender, EventArgs e)
        {
            //when we rename a item our EditableListBox returns a serialized string in a event.
            //we will deserialize this first and then scan for the old enum we will clone every data
            //after that we remove the enum from both lists the listbox in this class, and in the HashSet from TeleportType
            //CustomTeleportType has a function to remove.

            String[] serialized = ((String)sender).Split(',');
            String before = serialized[0];
            String after = serialized[1];

            String name = "";
            String voice = "";
            String x = "";
            String y = "";
            String z = "";

            foreach(CustomTeleportType type in this.types)
            {
                if(type.GetName() == before)
                {
                    name = after;
                    voice = type.getVoiceName();
                    x = type.GetX();
                    y = type.GetY();
                    z = type.GetZ();
                    this.types.Remove(type);
                    type.Remove();
                    break;
                }
            }

            CustomTeleportType newtype = new CustomTeleportType(name, voice, x, y, z);
            this.types.Add(newtype);

        }

        private void workercomplete(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Error != null)
            {
                this.worker.RunWorkerAsync();
            }
        }

        private void backgroundworkerevent(object sender, DoWorkEventArgs e)
        {

            while (true)
            {

                Location loc = this.trainer.GetPlayer().GetCurrentLocation();

                Thread.Sleep(100);

                String x = loc.GetX()+"";
                String y = loc.GetY()+"";
                String z = loc.GetZ()+"";

                this.xyzcoordlabel.Invoke(new Action(() => this.xyzcoordlabel.Text = "x: " + x + " y: " + y + " z: " + z));
            }
        }

        public CustomTeleportType[] GetTeleportTypesFromFile()
        {

            if (File.Exists(this.datafolder + @"\borderlands2trainer\locations.ini"))
            {
                String[] lines = File.ReadAllLines(this.datafolder + @"\borderlands2trainer\locations.ini");
                foreach (String line in lines)
                {
                    //deserialize locations...
                    String[] data = line.Split('|');
                    String name = data[0];
                    String voice = data[1];
                    String x = data[2];
                    String y = data[3];
                    String z = data[4];

                    this.types.Add(new CustomTeleportType(name, voice, x, y , z, true));

                }
                return this.types.ToArray();
            }
            throw new Exception("no file defined!");
        }

        public void makeMoveAble(Control c)
        {
            c.MouseDown += new MouseEventHandler(moveableWindowHoldEvent);
            c.MouseUp += new MouseEventHandler(moveableWindowUpEvent);
            c.MouseMove += new MouseEventHandler(moveableWindowMoveEvent);

        }

        private void moveableWindowMoveEvent(object sender, MouseEventArgs e)
        {
            if (this.isMouseClicked)
            {

                this.Location = new Point((this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void moveableWindowHoldEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Cursor = this.grab;
                this.isMouseClicked = true;
                this.lastLocation = e.Location;
            }
        }

        private void moveableWindowUpEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Cursor = this.ungrab;
                this.isMouseClicked = false;
            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", (datafolder + "/borderlands2trainer").Replace("/", @"\"));

        }

        private void Button4_Click(object sender, EventArgs e)
        {
            this.deletebtn.Enabled = false;
            if(this.teleportlist.SelectedItem != null)
            {
                
                //get the teleport type first :)
                foreach(CustomTeleportType type in types)
                {
                    if(type.GetName() == (String)this.teleportlist.SelectedItem)
                    {
                        types.Remove(type);
                        type.Remove();
                        break;
                    }
                }

                this.teleportlist.Items.Remove(this.teleportlist.SelectedItem);
            } else
            {
                MessageBox.Show("nothing to delete", "error");
            }
            this.deletebtn.Enabled = true;
        }

        private void Savelistbtn_Click(object sender, EventArgs e)
        {
            this.savelistbtn.Enabled = false;
            if(this.teleportlist.Items.Count > 0)
            {
                StringBuilder build = new StringBuilder();

                foreach(CustomTeleportType type in this.types)
                {
                        build.AppendLine(type.GetName() + "|" + type.getVoiceName() + "|" + type.GetX() + "|" + type.GetY() + "|" + type.GetZ());
                }

                if (File.Exists(this.datafolder + @"\borderlands2trainer\locations.ini"))
                {
                    File.Delete(this.datafolder + @"\borderlands2trainer\locations.ini");
                }

                File.WriteAllText(this.datafolder + @"\borderlands2trainer\locations.ini", build.ToString());

            } else
            {
                if (File.Exists(this.datafolder + @"\borderlands2trainer\locations.ini"))
                {
                    File.Delete(this.datafolder + @"\borderlands2trainer\locations.ini");
                }
                else
                {
                    MessageBox.Show("nothing to save!", "error!");
                }
            }
            this.savelistbtn.Enabled = true;
        }

        private void Loadlistbtn_Click(object sender, EventArgs e)
        {
            this.loadlistbtn.Enabled = false;
            if (!Directory.Exists(datafolder))
            {
                Directory.CreateDirectory(datafolder);
                Directory.CreateDirectory(datafolder + @"\borderlands2trainer");
            }

            this.teleportlist.Items.Clear();

            try
            {
                this.types = this.GetTeleportTypesFromFile().ToHashSet();
                foreach (CustomTeleportType type in types)
                {
                   this.teleportlist.Items.Add(type.GetName());
                }
            }
            catch (Exception)
            {
                MessageBox.Show("no location file found!", "error");
            }
            this.loadlistbtn.Enabled = true;
        }

        private void Addbtn_Click(object sender, EventArgs e)
        {
            this.addbtn.Enabled = false;
            String name = this.savedatanametextbox.Text;
            String voice = this.savedatavoicetextbox.Text;

            if (name != null && voice != null && !name.Contains("|") && !voice.Contains("|"))
            {

                String x = this.trainer.GetPlayer().GetCurrentLocation().GetX()+"";
                String y = this.trainer.GetPlayer().GetCurrentLocation().GetY() + "";
                String z = this.trainer.GetPlayer().GetCurrentLocation().GetZ() + "";


                CustomTeleportType custom = new CustomTeleportType(name, voice, x, y, z);

                this.types.Add(custom);
                this.teleportlist.Items.Add(custom.GetName());

            } else
            {
                MessageBox.Show("unable to save this location!, name or voice was empty or it contained | symbols", "error");
            }
            this.addbtn.Enabled = true;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.button1.Enabled = false;
            this.win.RebuildTeleportList();
            this.button1.Enabled = true;
        }
    }
}
