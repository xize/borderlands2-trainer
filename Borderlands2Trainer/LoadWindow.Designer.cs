﻿namespace Borderlands2Trainer
{
    partial class LoadWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadWindow));
            this.loadprogress = new System.Windows.Forms.Label();
            this.loadingani = new System.Windows.Forms.PictureBox();
            this.version = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.loadingani)).BeginInit();
            this.SuspendLayout();
            // 
            // loadprogress
            // 
            this.loadprogress.AutoSize = true;
            this.loadprogress.BackColor = System.Drawing.Color.Transparent;
            this.loadprogress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadprogress.ForeColor = System.Drawing.Color.LightBlue;
            this.loadprogress.Location = new System.Drawing.Point(81, 201);
            this.loadprogress.Name = "loadprogress";
            this.loadprogress.Size = new System.Drawing.Size(42, 17);
            this.loadprogress.TabIndex = 3;
            this.loadprogress.Text = "{0}%";
            // 
            // loadingani
            // 
            this.loadingani.BackColor = System.Drawing.Color.Transparent;
            this.loadingani.BackgroundImage = global::Borderlands2Trainer.Properties.Resources.ezgif_4_2953749d2ff8;
            this.loadingani.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.loadingani.Location = new System.Drawing.Point(35, 187);
            this.loadingani.Name = "loadingani";
            this.loadingani.Size = new System.Drawing.Size(40, 40);
            this.loadingani.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loadingani.TabIndex = 4;
            this.loadingani.TabStop = false;
            // 
            // version
            // 
            this.version.BackColor = System.Drawing.Color.Black;
            this.version.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.version.ForeColor = System.Drawing.Color.White;
            this.version.Location = new System.Drawing.Point(415, 201);
            this.version.Name = "version";
            this.version.ReadOnly = true;
            this.version.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.version.Size = new System.Drawing.Size(73, 21);
            this.version.TabIndex = 5;
            this.version.Text = "Version: {0}";
            // 
            // LoadWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Borderlands2Trainer.Properties.Resources.loadingscr2_new2;
            this.ClientSize = new System.Drawing.Size(500, 234);
            this.Controls.Add(this.version);
            this.Controls.Add(this.loadingani);
            this.Controls.Add(this.loadprogress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoadWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoadWindow";
            this.Load += new System.EventHandler(this.LoadWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.loadingani)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label loadprogress;
        private System.Windows.Forms.PictureBox loadingani;
        private System.Windows.Forms.RichTextBox version;
    }
}