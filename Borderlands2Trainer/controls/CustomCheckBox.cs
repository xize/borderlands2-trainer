﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer.controls
{
    public class CustomCheckBox : UserControl
    {
        private Panel ball;
        private bool isChecked = false;

        public event EventHandler CustomCheckBoxChange;

        public CustomCheckBox()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.ball = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // ball
            // 
            this.ball.BackgroundImage = global::Borderlands2Trainer.Properties.Resources.checkboxball_black1;
            this.ball.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ball.Location = new System.Drawing.Point(5, 2);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(8, 8);
            this.ball.TabIndex = 0;
            this.ball.Click += new System.EventHandler(this.onClickChange);
            // 
            // CustomCheckBox2
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::Borderlands2Trainer.Properties.Resources.checkbox_black1;
            this.Controls.Add(this.ball);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MaximumSize = new System.Drawing.Size(23, 12);
            this.MinimumSize = new System.Drawing.Size(23, 12);
            this.Name = "CustomCheckBox2";
            this.Size = new System.Drawing.Size(23, 12);
            this.Click += new System.EventHandler(this.onClickChange);
            this.ResumeLayout(false);

        }

        protected virtual void OnCustomCheckBoxChange(object sender)
        {
            EventHandler handler = CustomCheckBoxChange;
            if(handler != null)
            {
                handler(sender, EventArgs.Empty);
            }
        }


        private void onClickChange(object sender, EventArgs e)
        {
            this.Checked = !this.Checked;
        }

        [Description("checks or unchecks the checkbox"), Category("Appearance")]
        public bool Checked
        {
            get
            {
                return this.isChecked;
            }
            set
            {
                this.isChecked = value;
                if (value)
                {
                    this.ball.Location = new System.Drawing.Point(12, 2);
                    this.ball.BackgroundImage = Properties.Resources.checkboxball_black_active1;
                    this.BackgroundImage = Properties.Resources.checkbox_black1;
                }
                else
                {
                    this.ball.BackgroundImage = Properties.Resources.checkboxball_black1;
                    this.ball.Location = new System.Drawing.Point(5, 2);
                    this.BackgroundImage = Properties.Resources.checkbox_black1;
                }
                this.OnCustomCheckBoxChange(this);
                this.Update();
            }
        }

    }

}
