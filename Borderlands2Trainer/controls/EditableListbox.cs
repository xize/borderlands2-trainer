﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer.controls
{
    class EditableListbox : ListBox
    {

        private TextBox editbox;
        private bool editable = true;

        private KeyPressEventHandler keypressh;
        private EventHandler doublelcickh;
        private KeyEventHandler keydownh;

        public event EventHandler EditChanged;

        public EditableListbox()
        {
            this.initialize();
        }

        public void initialize()
        {

            this.keypressh = new KeyPressEventHandler(this.listBox1_KeyPress);
            this.doublelcickh = new EventHandler(this.listBox1_DoubleClick);
            this.keydownh = new KeyEventHandler(this.listBox1_KeyDown);


            this.KeyPress += this.keypressh;
            this.DoubleClick += this.doublelcickh;
            this.KeyDown += this.keydownh;

            this.editbox = new TextBox();
            this.editbox.Hide();
            this.editbox.KeyPress += new KeyPressEventHandler(EditOver);
            this.editbox.LostFocus += new EventHandler(FocusOver);

        }

        [Description("allows the listbox to be editable"), Category("Appearance")]
        public bool Editable
        {
            get
            {
                return this.editable;
            }
            set
            {
                if(value)
                {
                    this.KeyPress += this.keypressh;
                    this.DoubleClick += this.doublelcickh;
                    this.KeyDown += this.keydownh;
                } else
                {
                    this.KeyPress -= this.keypressh;
                    this.DoubleClick -= this.doublelcickh;
                    this.KeyDown -= this.keydownh;
                }
                this.editable = value;
            }
        }

        protected virtual void OnEditChanged(Object sender, EventArgs e)
        {
            EventHandler handler = EditChanged;
            if(EditChanged != null)
            {
                handler(sender, e);
            }
        }

        private void listBox1_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                CreateEditBox(sender);
        }
        private void listBox1_DoubleClick(object sender, System.EventArgs e)
        {
            CreateEditBox(sender);
        }

        private void listBox1_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyData == Keys.F2)
                CreateEditBox(sender);
        }
        private void CreateEditBox(object sender)
        {
            int itemSelected = this.SelectedIndex;
            Rectangle r = this.GetItemRectangle(itemSelected);
            string itemText = (string)this.Items[itemSelected];
            editbox.Location = new System.Drawing.Point(r.X, r.Y);
            editbox.Size = new System.Drawing.Size(r.Width - 10, r.Height);
            editbox.Show();
            this.Controls.AddRange(new System.Windows.Forms.Control[] { this.editbox });
            editbox.Text = itemText;
            editbox.Focus();
            editbox.SelectAll();
            editbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EditOver);
            editbox.LostFocus += new System.EventHandler(this.FocusOver);
        }

        private void FocusOver(object sender, System.EventArgs e)
        {
            String before = (String)this.Items[this.SelectedIndex];
            String after = editbox.Text;
            this.Items[this.SelectedIndex] = editbox.Text;
            OnEditChanged(before+","+after, EventArgs.Empty);
            editbox.Hide();
        }

        private void EditOver(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                String before = (String)this.Items[this.SelectedIndex];
                String after = editbox.Text;

                this.Items[this.SelectedIndex] = this.editbox.Text;

                OnEditChanged(before+","+after, EventArgs.Empty);

                this.editbox.Hide();
            }
        }
    }
}
