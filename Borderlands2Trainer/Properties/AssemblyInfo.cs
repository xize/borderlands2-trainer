﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Borderlands2Trainer")]
[assembly: AssemblyDescription("a trainer for borderlands 2")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("0c3")]
[assembly: AssemblyProduct("Borderlands2Trainer")]
[assembly: AssemblyCopyright("© xize cheats 2019-2024 all rights reserved")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("95b8a491-9649-468f-9ea7-1e9433742a5c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.2.349")]
[assembly: AssemblyFileVersion("2.0.2.349")]
