﻿
using Borderlands2Trainer;
using Borderlands2Trainer.cheats;
using Borderlands2Trainer.util;
using Memory;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    public class Trainer
    {

        private Mem m;
        private Window win;
        private bool hooked = false;
        private BackgroundWorker worker = new BackgroundWorker();

        //cheats

        private IPlayer player;

        //end cheats

        public Trainer(Window win, Mem m)
        {
            this.win = win;
            this.m = m;

            //instance the cheats.
            this.InitializeCheats(win, m);
        }

        public void InitializeCheats(Form win, Mem m)
        {
            //this.shieldcheat = new ShieldCheat(m);
            //this.godmodecheat = new GodmodeCheat(this.win, this.m);
            //this.grenadecheat = new GrenadeCheat(this.m);

            //TEST
            this.player = new PlayerModel(this.m);
            //END TEST :)

        }

        public IPlayer GetPlayer()
        {
            return this.player;
        }

        public bool OpenGame()
        {

            int PID = this.m.getProcIDFromName("Borderlands2.exe");

            if (this.hooked && PID > 0)
                return true;

            if (PID > 0)
            {
                if(!this.hooked)
                {
                    bool bol = this.m.OpenProcess(PID);
                    if(bol)
                    {
                        this.win.hookedstatus.Invoke(new Action(() => this.win.hookedstatus.ForeColor = Color.Green));
                        this.win.hookedstatus.Invoke(new Action(() => this.win.hookedstatus.Text = "success"));
                        this.hooked = true;
                        Console.Beep(500, 100);
                        Console.Beep(200, 100);
                        Console.Beep(500, 100);
                        return true;
                    }
                }
            } else {
                    this.win.hookedstatus.Invoke(new Action(() => this.win.hookedstatus.ForeColor = Color.Red));
                    this.win.hookedstatus.Invoke(new Action(() => this.win.hookedstatus.Text = "false"));
                    MessageBox.Show("start borderlands first or the trainer will not work !");
                    this.win.Invoke(new Action(() => this.win.Exit()));
                    this.hooked = false;
            }
            return false;
        }


        public void Start()
        {
            if(!worker.IsBusy)
            {
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OnBackgroundWorkerCompletedEvent);
                worker.DoWork += new DoWorkEventHandler(OnDoWorkEvent);
                worker.RunWorkerAsync();
            }
        }

        private void OnDoWorkEvent(Object sender, DoWorkEventArgs e)
        {
            while(true)
            {
                this.OpenGame();


                if (!this.hooked)
                    return;

                if (this.win.godmodecheckbox.Checked)
                {

                    this.player.SetHealth(this.player.GetMaxHealth());
                    //this.godmodecheat.RunCheat();
                }

                //invoke wether cheat is enabled or not :)
                if(this.win.shieldcheckbox.Checked)
                {
                    this.player.SetShield(this.player.GetMaxShield());
                    //this.shieldcheat.RunCheat();
                }

                //invoke wether cheat is enabled or not :)
                if (this.win.grandecheckbox.Checked)
                {
                    this.player.SetGrenades(10);
                    //this.grenadecheat.RunCheat();
                }

                if(this.win.badassgeneratorcheckbox.Checked)
                {
                    this.player.GenerateBaddassToken();
                } else
                {
                    this.player.DisableBadassToken();
                }
            }
        }

        private void OnBackgroundWorkerCompletedEvent(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Error is Exception)
            {


                int currentval = Int32.Parse(this.win.errorstatus.Text);

                this.win.errorstatus.Text = ""+(currentval + 1);
                this.win.errorstatus.ForeColor = Color.Red;

                //use this for debugging the game.

                if (Window.IsDebug())
                {
                    ErrorWindow errwin = new ErrorWindow(e.Error.StackTrace, "Exception");
                    errwin.ShowDialog();
                    return;
                }

                this.Start();
            }
        }

    }
}
