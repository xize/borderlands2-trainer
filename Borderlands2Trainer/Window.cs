﻿using Borderlands2Trainer.cheats;
using Borderlands2Trainer.util;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    public partial class Window : Form
    {

        private static bool debug;
        private System.Windows.Forms.Timer timer;
        private bool isMouseClicked = false;
        private Point lastLocation;
        private Borderlands2TrainerHelp helpwindw = new Borderlands2TrainerHelp();
        private CompileDebugWindow debugwindow;

        private Trainer trainer;

        private Cursor ungrab;
        private Cursor grab;

        private Mem m = new Mem();

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public Window(bool debugg = false)
        {
            InitializeComponent();
            
            this.ungrab = new Cursor(Properties.Resources.grabicon1.GetHicon());
            this.grab = new Cursor(Properties.Resources.grabicon2.GetHicon());


            this.debugwindow = new CompileDebugWindow(this, this.m);
            debug = debugg;
            if (debugg)
            {
                this.debugwindow.Show();
            }

            this.Opacity = 0.0;
            this.timer = new System.Windows.Forms.Timer();
            this.timer.Interval = 100;
            this.timer.Tick += new EventHandler(tickevent);
            this.timer.Start();
        }

        private void Window2_Load(object sender, EventArgs e)
        {
            if (debug)
            {
                Speech.Speak("Running trainer in debug modus!");
            }

            this.FullyLoaded += new EventHandler(formcompleted);
            CornerUtil.MakeRoundedCorners(this, 20);

            this.version.Text = String.Format(this.version.Text, Application.ProductVersion);
            this.cursorholder.Cursor = this.ungrab;
            this.makeMoveAble(this.cursorholder);
            this.instakillbtn.Click += new EventHandler(instakillchanged);
            this.FormClosing += new FormClosingEventHandler(onformclose);
            ToolStripItemCollection col = this.playerToolStripMenuItem1.DropDownItems;

            this.trainer = new Trainer(this, m);
            trainer.Start();
            this.tpmanager = new TeleportManager(this, trainer);

            foreach (ToolStripMenuItem item in col)
            {
                if (item.CheckOnClick)
                {
                    item.CheckedChanged += new EventHandler(menucheat);
                }
            }

            //add teleport stuff
            foreach (TeleportType tel in TeleportType.Values())
            {
                this.teleportbox.Items.Add(tel.GetName());

                ToolStripMenuItem item = new ToolStripMenuItem(tel.GetName());
                item.BackColor = Color.Black;
                item.ForeColor = Color.White;
                item.Click += new EventHandler(setlocationtpmenu);
                this.teleportitem.DropDownItems.Add(item);

            }

            this.teleportbox.Text = (String)teleportbox.Items[0];

            this.RegisterHotKeys();

            this.flycheckbox.CustomCheckBoxChange += new EventHandler(FlyChangeEvent);

        }

        private void onformclose(object sender, FormClosingEventArgs e)
        {
            this.Exit();
        }

        private void tickevent(object sender, EventArgs e)
        {
            if(this.Opacity == 1)
            {
                this.timer.Stop();

                this.OnFullyLoaded(null);

            } else
            {
                this.Opacity = this.Opacity+0.1;
            }
        }

        public void Exit()
        {
            Application.Exit();
        }

        public void makeMoveAble(Control c)
        {
            c.MouseDown += new MouseEventHandler(moveableWindowHoldEvent);
            c.MouseUp += new MouseEventHandler(moveableWindowUpEvent);
            c.MouseMove += new MouseEventHandler(moveableWindowMoveEvent);

        }


        public static bool IsDebug()
        {
            return debug;
        }

        private void moveableWindowMoveEvent(object sender, MouseEventArgs e)
        {
            if (this.isMouseClicked)
            {

                this.Location = new Point((this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void moveableWindowHoldEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.cursorholder.Cursor = this.grab;
                this.isMouseClicked = true;
                this.lastLocation = e.Location;
            }
        }

        private void moveableWindowUpEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.cursorholder.Cursor = this.ungrab;
                this.isMouseClicked = false;
            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        public Trainer GetTrainer()
        {
            return this.trainer;
        }

        private void Label6_Click(object sender, EventArgs e)
        {
            Point currentpoint = this.Location;
            this.helpwindw.Opacity = 0.8;
            this.helpwindw.Location = new Point(currentpoint.X+20, currentpoint.Y+80);
            this.helpwindw.ShowDialog();
        }

        private void CreditsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Point currentpoint = this.Location;
            this.helpwindw.Opacity = 0.8;
            this.helpwindw.Location = new Point(currentpoint.X + 20, currentpoint.Y + 80);
            this.helpwindw.ShowDialog();
        }

        private void GithubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", "https://gitlab.com/xize/borderlands2-trainer/-/releases");
        }

        private void ScanAobClick(object sender, EventArgs e)
        {
            ScanAOB aob = new ScanAOB(this, this.GetTrainer().GetPlayer());
            aob.Show();
            Point currentpoint = this.Location;
            currentpoint.Offset(60, 100);
            aob.Location = currentpoint;
        }

        private void CheckForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateWindow upwindow = new UpdateWindow();
            Point loc = this.Location;
            loc.Offset(60, 100);
            upwindow.Show();
            upwindow.Location = loc;
        }
    }
}
