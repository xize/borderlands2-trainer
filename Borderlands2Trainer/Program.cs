﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    static class Program
    {


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length == 0)
            {

                
                Application.Run(new LoadWindow());
            } else if(args.Length == 1) {
                if(args[0].ToLower() == "-debug")
                {
                    Application.Run(new LoadWindow(true));
                    Console.ReadKey();
                } else
                {
                    MessageBox.Show("invalid argument!");
                }
            } else
            {
                MessageBox.Show("invalid argument!");
            }
        }

    }
}
