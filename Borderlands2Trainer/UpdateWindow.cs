﻿using gitlabupdater;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    public partial class UpdateWindow : Form
    {
        private BackgroundWorker worker;
        private Updater updater;

        public UpdateWindow()
        {
            InitializeComponent();
        }

        private void UpdateWindow_Load(object sender, EventArgs e)
        {
            //"1.9.6.228"
            this.updater = new Updater("xize/borderlands2-trainer", Application.ProductVersion);
            this.worker = new BackgroundWorker();
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;

            //configure events:
            this.worker.DoWork += new DoWorkEventHandler(processwork);
            this.worker.ProgressChanged += new ProgressChangedEventHandler(processcchanged);
            this.worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(progresscompleted);
            this.worker.RunWorkerAsync();
        }

        private void processwork(object sender, DoWorkEventArgs e)
        {
            this.worker.ReportProgress(20);
            if (this.updater.Verify())
            {
                this.worker.ReportProgress(50);

                this.updater.Download(worker);
                this.worker.ReportProgress(100);
            }
            else
            {
                this.worker.ReportProgress(100);
            }
        }

        private void progresscompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Cancelled)
            {
                MessageBox.Show("update canceled!", "caneled", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else if(!e.Cancelled)
            {
                if(this.updater.NewVersion != "" && this.updater.NewVersion != null)
                {
                    MessageBox.Show("found update with name: "+this.updater.NewVersion+ "!", "update found!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                } else
                {
                     MessageBox.Show("your software is up to date!", "software is up-to-date!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            this.Close();
        }

        private void processcchanged(object sender, ProgressChangedEventArgs e)
        {
                if(e.ProgressPercentage == 50)
                {
                    this.lastreleaselabel.Text = "last release: v"+this.updater.NewVersion;
                    
                }
                this.progress.Value = e.ProgressPercentage;
                this.progresslabel.Text = "progress: "+e.ProgressPercentage+"%";
            
        }

        private void Cancelbtn_Click(object sender, EventArgs e)
        {
            this.cancelbtn.Enabled = false;
            this.worker.CancelAsync();

        }
    }
}
