﻿using Borderlands2Trainer.cheats;
using Borderlands2Trainer.util;
using gitlabupdater;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    partial class Window
    {
        private AutoUpdateWindowConfirm upwindow = new AutoUpdateWindowConfirm();
        private Updater updater = new Updater("xize/borderlands2-trainer", Application.ProductVersion);
        private BackgroundWorker w;
        private bool updatefound = false;

        private void formcompleted(object sender, EventArgs e)
        {
            if (debug)
            {
                this.debugwindow.Focus();
            }
            this.SetupUpdate();
        }

        //setup this method once per day, so we are not giving huge hits to gitlab :)
        private void SetupUpdate()
        {
            String datafolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/0c3";
            if (!Directory.Exists(datafolder))
            {
                Directory.CreateDirectory(datafolder);
            }

            if (!Directory.Exists(datafolder + "/borderlands2trainer"))
            {
                Directory.CreateDirectory(datafolder + "/borderlands2trainer");
            }

            if (File.Exists(datafolder + "/borderlands2trainer/auto-update.dat"))
            {
                long dat = long.Parse(File.ReadAllText(datafolder + "/borderlands2trainer/auto-update.dat"));
                DateTime time = new DateTime(dat);
                DateTime now = DateTime.Now;
                if (now.Subtract(time).TotalDays <= 7)
                {
                  return;
                }
            }

            File.WriteAllText(datafolder + "/borderlands2trainer/auto-update.dat", DateTime.Now.Ticks + "");


            this.w = new BackgroundWorker();
            w.WorkerReportsProgress = true;
            w.DoWork += new DoWorkEventHandler(AutoUpdateDoWork);
            w.ProgressChanged += new ProgressChangedEventHandler(AutoUpdateProgress);
            w.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AutoUpdateCompleted);
            w.RunWorkerAsync();
        }

        private void AutoUpdateDoWork(object sender, DoWorkEventArgs e)
        {
            w.ReportProgress(30);
            if(updater.Verify())
            {
                w.ReportProgress(50);
            } else
            {
                for (int i = 0; i < 180; i++)
                {
                    //check for different update :)
                    this.updater = new Updater("xize/borderlands2-trainer", Application.ProductVersion.Replace("349", 349+i+""));
                    if (updater.Verify())
                    {
                        w.ReportProgress(50);
                    }
                }
            }
            w.ReportProgress(100);
            Thread.Sleep(1200);
        }

        private void AutoUpdateProgress(object sender, ProgressChangedEventArgs e)
        {
            if(e.ProgressPercentage == 50)
            {
                this.updatefound = !this.updatefound;
            }
        }

        private void AutoUpdateCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(this.updatefound)
            {
                upwindow.NewVersion = this.updater.NewVersion;
                upwindow.OldVersion = Application.ProductVersion;
                upwindow.Updater = this.updater;
                Point loc = this.Location;
                loc.Offset(90, 150);
                upwindow.Show();
                upwindow.Location = loc;
                upwindow.Focus();
            }
        }

        private void instakillchanged(object sender, EventArgs e)
        {

            Speech.Speak(this.mute, "instakill has been set");

            IPlayer p = this.GetTrainer().GetPlayer();
            IDmg dmg = (IDmg)p;

            dmg.SetAttack();
        }

        private void FlyChangeEvent(object sender, EventArgs e)
        {
            if (this.flycheckbox.Checked)
            {
                this.hotkey.RegisterKey(Keys.Space);
            }
            else
            {
                this.UnRegisterHotKeys();
                this.RegisterHotKeys();
            }
        }

        private void setlocationtpmenu(object sender, EventArgs e)
        {
            String name = ((ToolStripMenuItem)sender).Text;

            this.teleportbox.Text = name;

        }

        private void Tpbutton_Click(object sender, EventArgs e)
        {
            this.DoTeleport();
        }

        private void Tpundo_Click(object sender, EventArgs e)
        {
            this.trainer.GetPlayer().UndoTeleport();
        }

        private void Teleportsetbtn_Click(object sender, EventArgs e)
        {
            this.tpmanager.StartPosition = FormStartPosition.CenterScreen;
            this.tpmanager.Show();
        }

        private void Moneybtn_Click(object sender, EventArgs e)
        {
            this.GetTrainer().GetPlayer().SetMoney(this.GetTrainer().GetPlayer().GetMoney() + Decimal.ToInt32(this.moneynumeric.Value));
        }

        private void Eridiumbtn_Click(object sender, EventArgs e)
        {
            this.GetTrainer().GetPlayer().SetEridium(this.GetTrainer().GetPlayer().GetEridium() + Decimal.ToInt32(this.eridiumnumeric.Value));
        }

        private void Goldkeybtn_Click(object sender, EventArgs e)
        {
            IGoldenKey key = (IGoldenKey)this.GetTrainer().GetPlayer();
            key.SetGoldenKeys(key.GetGoldenKeys() + Decimal.ToInt32(this.goldkeynumeric.Value));
        }

        private void Seraphbtn_Click(object sender, EventArgs e)
        {
            this.GetTrainer().GetPlayer().SetSeraphCrystals(this.GetTrainer().GetPlayer().GetSeraphCrystals() + Decimal.ToInt32(this.seraphnumeric.Value));
        }

        private void Torguebtn_Click(object sender, EventArgs e)
        {
            this.GetTrainer().GetPlayer().SetTorgueTokens(this.GetTrainer().GetPlayer().GetTorgueTokens() + Decimal.ToInt32(this.torguenuumeric.Value));
        }

        private void Levelbtn_Click(object sender, EventArgs e)
        {
            this.GetTrainer().GetPlayer().SetLevel(Decimal.ToInt32(this.levelnumeric.Value));
        }

    }
}
