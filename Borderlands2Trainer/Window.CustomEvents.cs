﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands2Trainer
{
    public partial class Window
    {

        public event EventHandler FullyLoaded;

        protected virtual void OnFullyLoaded(EventArgs e)
        {
            EventHandler handler = FullyLoaded;
            handler?.Invoke(handler, e);
        }

    }
}
