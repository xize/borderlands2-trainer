﻿using Borderlands2Trainer.util;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer.cheats
{
    class PlayerModel : ModelCheat, IPlayer, IDmg, IGoldenKey
    {

        //private String aob_signature = "18 60 ?? ?? ?? ?? 00 00 F4 01 00 00 18 ?? ?? ?? ?? ?? 00 00 D0 07 00 00 18 68 ?? ?? ?? 00 00 00 F4 01 00 00 18 ?0 ?? ?? ?? 00 00 00 D0 07 00 00 18 70 ?? ?? 00 00 00 00 F4 01 00 00 18 ?? ?? ?? ?? 00";
        private String aob_signature = "?8 1? ?? 6? 00 00 00 00 04 00 00 00 01 00 00 00 ?? ?? 00 00 00 00 00 00 0? 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 70 57 00 00 00 00 00 00 01 00 00 00 91 55 00 00 00 00 00 00 01 00 00 00 18 96 ?? 0F 18 B0";
        private bool isbadasstoken = false;
        private Location lastLocation;


        //private String goldenkey_signature = "?? ?? ?? ?? ?? ?? 00 00 ?? 0? 00 00 18 60 ?? ?? ?? 00 00 00 ?? ?? 00 00 18 ?? ?? ?? ?? ?? 00 00 D0 07 00 00 18 68 ?? ?? 00 00 00 00 F4 01 00 00 18 ?? ?? ?? ?? 00 00 00 D0 07 00 00 18 70 ?? ?? 00 00 00 00 ?? 01 00 00 18 ?? ?? ?? ?? 00 00 00";
        private String goldenkey_signature = "?8 C? ?? ?? ?? ?? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?? C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?? ?? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 C? ?? ?? ?8 ?? ?? ?? ?8 ?? ?? ?? ?8 ?? ?? ?? ?8 ?? ?? ?? ?8 ?? ?? ?? ?8 ?? ?? ?? ?8 ?? ?? ?? ?8 B? ?? ?? ?8 B? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?8 ?? ?? ?? ?8 ?? ?? 5? ?? 2A ?? ??";

        private String goldenkey_base = "Borderlands2.exe+0x016949CC";

        private String attack_base = "Borderlands2.exe+0x1633250";

        //old base: v228 Borderlands2.exe+0x0164C458

        public PlayerModel(Mem m) : base("Borderlands2.exe+0x016B61AC", m){}

        public float GetCurrentHealth()
        {
            String offsets = "0x0,0x194,0x14,0x14,0x3AC,0x6C";
            float value = this.GetMemory().readFloat(this.GetBaseAddress() +"," + offsets);
            return value;
        }

        public int GetMaxHealth()
        {
            String offsets = "0x0,0x28,0x2A8,0x80,0xA4,0x378";
            int value = this.GetMemory().readInt(this.GetBaseAddress() +","+ offsets);
            return value;
        }

        public void SetHealth(float val)
        {
            String offsets = "0x0,0x194,0x14,0x14,0x3AC,0x6C";
            this.GetMemory().writeMemory(this.GetBaseAddress() +","+ offsets, "float", val + "");
        }


        public float GetCurrentShield()
        {
            String offsets = "0x0,0xE0,0x36C,0x1C4,0x4,0x3B8,0x6C";
            float value = this.GetMemory().readFloat(this.GetBaseAddress()+","+ offsets);
            return value;
        }

        public int GetMaxShield()
        {
            //String offsets = "0x30,0x1D0,0x1EC,0xDC,0x778";
            String offsets = "0x0,0xE0,0x28,0x2A8,0x0,0x190,0x778";
            int value = this.GetMemory().readInt(this.GetBaseAddress()+","+ offsets);
            return value;
        }

        public void SetShield(float val)
        {
            String offsets = "0x0,0xE0,0x36C,0x1C4,0x4,0x3B8,0x6C";
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+ offsets, "float", val+"");
        }


        public void SetGrenades(float val)
        {
            String offsets = "0x0,0xAC,0x50,0x340,0x1B0,0x28,0x190,0x6C";
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+offsets, "float", val+"");
        }

        public void GenerateBaddassToken()
        {
            this.isbadasstoken = true;
            this.SetHealth(long.MaxValue);
            Thread.Sleep(10);
            this.SetHealth(this.GetMaxShield());
        }

        public void DisableBadassToken()
        {
            if(this.isbadasstoken)
            {
                this.isbadasstoken = !this.isbadasstoken;
                this.SetHealth(this.GetMaxHealth());
                this.SetHealth(this.GetMaxHealth());
            }
        }

        public Location GetCurrentLocation()
        {
            String xoffset = "0x0,0x194,0x28,0x2A8,0xC4,0xA4,0x60";
            String yoffset = "0x0,0x28,0x3C,0x11C,0x18C,0xDC,0x68";
            String zoffset = "0x0,0x100,0x28,0x2A8,0x108,0x1EC,0x64";

            float xcoord = this.GetMemory().readFloat(this.GetBaseAddress() + "," + xoffset);
            float ycoord = this.GetMemory().readFloat(this.GetBaseAddress() + "," + yoffset);
            float zcoord = this.GetMemory().readFloat(this.GetBaseAddress() + "," + zoffset);

            return new Location(xcoord, ycoord, zcoord);
        }

        public void TeleportJump()
        {
            String yoffset = "0x0,0x28,0x3C,0x11C,0x18C,0xDC,0x68";

            float y = this.GetMemory().readFloat(this.GetBaseAddress()+","+yoffset, "", false) + 100;
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+yoffset, "float", y+"");
        }

        public void TeleportTo(TeleportType type)
        {
            String xoffset = "0x0,0x194,0x28,0x2A8,0xC4,0xA4,0x60";
            String yoffset = "0x0,0x28,0x3C,0x11C,0x18C,0xDC,0x68";
            String zoffset = "0x0,0x100,0x28,0x2A8,0x108,0x1EC,0x64";

            this.lastLocation = this.GetCurrentLocation();

            this.GetMemory().writeMemory(this.GetBaseAddress()+","+xoffset, "float", type.GetX());
            this.GetMemory().writeMemory(this.GetBaseAddress() + ","+yoffset, "float", type.GetY());
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+zoffset, "float", type.GetZ());
        }

        public void UndoTeleport()
        {

            if(this.lastLocation == null)
            {
                Console.Beep();
                return;
            }

            String xoffset = "0x0,0x194,0x28,0x2A8,0xC4,0xA4,0x60";
            String yoffset = "0x0,0x28,0x3C,0x11C,0x18C,0xDC,0x68";
            String zoffset = "0x0,0x100,0x28,0x2A8,0x108,0x1EC,0x64";

            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + xoffset, "float", this.lastLocation.GetX()+"");
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + yoffset, "float", this.lastLocation.GetY() + "");
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + zoffset, "float", this.lastLocation.GetZ() + "");
        }

        public void SetMoney(int money)
        {
            String offsets = "0x0,0x76C,0x28,0x34C,0x108,0x194,0x2A0";
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "int", money+"");
        }

        public int GetMoney()
        {
            String offsets = "0x0,0x76C,0x28,0x34C,0x108,0x194,0x2A0";
            int money = this.GetMemory().readInt(this.GetBaseAddress()+","+offsets);
            return money;
        }

        public int GetEridium()
        {
            String offsets = "0x0,0xAC,0xA4,0x28,0x3C,0x94,0x2B4";
            int n = this.GetMemory().readInt(this.GetBaseAddress()+","+offsets);
            return n;
        }

        public void SetEridium(int amount)
        {
            String offsets = "0x0,0xAC,0xA4,0x28,0x3C,0x94,0x2B4";
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "int", amount + "");
        }

        //multi level pointer, result pointer is a byte.
        public int GetGoldenKeys()
        {
            //get reference address from pointer.
            String offsets = "0x8,0x18C,0x60,0xAC,0x68,0x74,0xCF0";
            int refptr = this.GetMemory().readInt(this.goldenkey_base+","+offsets);
            long hexaddr = Convert.ToInt64(refptr);
            long newval = hexaddr + 0x1;
            String address = Convert.ToString(newval, 16).ToUpper();

            int amount = this.GetMemory().readByte(address);
            
            return amount;
        }

        public void SetGoldenKeys(int amount)
        {
            //get reference address from pointer.
            String offsets = "0x8,0x18C,0x60,0xAC,0x68,0x74,0xCF0";
            int refptr = this.GetMemory().readInt(this.goldenkey_base + "," + offsets);
            long hexaddr = Convert.ToInt64(refptr);
            long newval = hexaddr + 0x1;


            String address = Convert.ToString(newval, 16).ToUpper();

            if(amount > 255)
            {
                Console.Beep(100, 800);
                return;
            }

            this.GetMemory().writeMemory(address, "int", amount+"");
           
        }

        public void SetSeraphCrystals(int amount)
        {
            String offsets = "0x0,0x28,0x2A8,0x3C,0x14,0x14,0x2C8";
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+offsets, "int", amount+"");
        }

        public int GetSeraphCrystals()
        {
            String offsets = "0x0,0x28,0x2A8,0x3C,0x14,0x14,0x2C8";
            int n = this.GetMemory().readInt(this.GetBaseAddress() + "," + offsets);
            return n;
        }

        public int GetTorgueTokens()
        {
            String offsets = "0x0,0x28,0x48,0x3C,0x94,0x2F0";
            int a = this.GetMemory().readInt(this.GetBaseAddress() + "," + offsets);
            return a;
        }

        public void SetTorgueTokens(int amount)
        {
            String offsets = "0x0,0x28,0x48,0x3C,0x94,0x2F0";
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "int", amount + "");
        }

        public void Fly()
        {
            String offsets = "0x0,0x194,0xA4,0x54,0x0,0x28,0x190,0x10C";

            float jumpstate = this.GetMemory().readFloat(this.GetBaseAddress()+","+offsets);

            if(jumpstate == 0)
            {
                this.TeleportJump();
            }

            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "float", 1000 + "");
        }

        public void SetLevel(int i)
        {
            String offsets = "0x0,0x28,0x48,0x2A8,0x4C,0x1A8,0x6C";
            int clevel = this.GetLevelXP(i);

            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "float", clevel+"");

        }

        /*
         How to engineer:

            1) Search the value as the normal approach.
            2) then add the debugger on the pointer and check what accesses the pointer
            3) do not fall for the last results with "inc", in total you will see around 8 results one of them have a different pointer
            4) search this pointer via hex search, and you will find one static pointer which is green and a bunch of fake pointers
            5) use this static pointer to calculate the result with a offset of 0x167C this make you enter the registry or base of the stats, add 0x1C and you are to the damage stat.
         * */
        public void SetAttack()
        {

            //first get the registry memory address :)

            int registry_offset = 0x167C;
            int ref_registry_addressi = this.GetMemory().readInt(this.attack_base);
            ref_registry_addressi = ref_registry_addressi + registry_offset;
            String ref_registry_address = Convert.ToString(ref_registry_addressi, 16).ToUpper();

            //get reference from the registry :)

            int result_offset = 0x1C;
            int ref_result_addressi = this.GetMemory().readInt(ref_registry_address);
            ref_result_addressi = ref_result_addressi + result_offset;
            String ref_result_adress = Convert.ToString(ref_result_addressi, 16).ToUpper();

            //now get the address we need to modify the damage

            this.GetMemory().writeMemory(ref_result_adress, "int", Int32.MaxValue + "");
        }

        public int GetLevelXP(int wantedlevel)
        {

            wantedlevel = wantedlevel - 1;


            /*
             * from: https://borderlands.fandom.com/wiki/Experience_Points
             * In Borderlands, Borderlands 2, and Borderlands: The Pre-Sequel,
             * the amount of experience necessary to reach level x equals ceil(60x2.8 − 60).
             */

            int[] levels =
            {
                0,
                358,
                1241,
                2850,
                5376,
                8997,
                13886,
                20208,
                28126,
                37798,
                49377,
                63016,
                78861,
                97061,
                117757,
                141092,
                167206,
                196238,
                228322,
                263595,
                302190,
                344238,
                389873,
                439222,
                492414,
                549578,
                610840,
                676325,
                746158,
                820463,
                899363,
                982980,
                1071435,
                1164850,
                1263343,
                1367034,
                1476041,
                1590483,
                1710476,
                1836137,
                1967582,
                2104926,
                2248285,
                2397772,
                2553501,
                2715586,
                2884139,
                3059273,
                3241098,
                3429728,
                3625271,
                3827840,
                4037543,
                4254491,
                4478792,
                4710556,
                4949890,
                5196902,
                5451701,
                5714393,
                5985086,
                6263885,
                6550897,
                6846227,
                7149982,
                7462266,
                7783184,
                8112840,
                8451340,
                8798786,
                9155282,
                9520932,
                9895840,
                10280105,
                10673832,
                11077123,
                11490079,
                11912803,
                12345396,
                12787958
            };

            return levels[wantedlevel];

        }

        private void CatogorizeBaseSpeak(String basename, Type clazz)
        {
            MethodInfo[] m = clazz.GetMethods();

            List<String> catdata = new List<string>();

            foreach (MethodInfo method in m)
            {
                bool attribfound = method.GetCustomAttributes(typeof(Cheat), false).Any();
                if (attribfound)
                {
                    Cheat c = (Cheat)method.GetCustomAttribute(typeof(Cheat), false);
                    catdata.Add(c.GetCheat);

                }

            }
            String cheatlist = String.Join(", ", catdata);

            Speech.SpeakSync("scanning for "+ basename +" base signature containing the following cheats: ");
            Speech.SpeakSync(cheatlist);
        }

        public override void PerformAOBScan(BackgroundWorker worker)
        {
            //begin player base scan
            worker.ReportProgress(10);
            if(worker.CancellationPending)
            {
                return;
            }
            Task<IEnumerable<long>> t1 = this.GetMemory().AoBScan(this.aob_signature, true, false);
            t1.ConfigureAwait(true);
            t1.Wait();
            long iaddress = t1.Result.FirstOrDefault();
            if (iaddress > 0)
            {

                String address = Convert.ToString(iaddress, 16).ToUpper();
                if (address != "0")
                {
                    worker.ReportProgress(20);
                    this.SetBaseAddress(address);
                    worker.ReportProgress(50);
                } else
                {
                    worker.ReportProgress(25);
                }
            } else
            {
                worker.ReportProgress(25);
            }
            //end player scan

            /*
            //begin instakkill scan
            worker.ReportProgress(30);
            if (worker.CancellationPending)
            {
                return;
            }
            Task<IEnumerable<long>> t2 = this.GetMemory().AoBScan(this.attack_signature, true, false);
            t2.ConfigureAwait(true);
            t2.Wait();
            long iaddress2 = t2.Result.FirstOrDefault();
            if (iaddress2 > 0)
            {

                String address2 = Convert.ToString(iaddress2, 16).ToUpper();
                if (address2 != "0")
                {
                    this.attackbase = address2;
                    worker.ReportProgress(50);
                } else
                {
                    worker.ReportProgress(55);
                }
            } else
            {
                worker.ReportProgress(55);
            }
            //end instakill scan
            */

            //begin golden key scan
            worker.ReportProgress(60);
            if (worker.CancellationPending)
            {
                return;
            }
            Task<IEnumerable<long>> t3 = this.GetMemory().AoBScan(this.goldenkey_signature, true, false);
            t3.ConfigureAwait(true);
            t3.Wait();
            long iaddress3 = t3.Result.FirstOrDefault();
            if (iaddress3 > 0)
            {

                String address3 = Convert.ToString(iaddress3, 16).ToUpper();
                if (address3 != "0")
                {
                    this.goldenkey_base = address3;
                    worker.ReportProgress(80);
                } else
                {
                    worker.ReportProgress(85);
                }
            } else
            {
                worker.ReportProgress(85);
            }

            //end golden key scan
            if (worker.CancellationPending)
            {
                return;
            }
            worker.ReportProgress(100);
            Thread.Sleep(2000);
        }

    }
}
