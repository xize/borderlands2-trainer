﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands2Trainer.cheats
{
    public interface IPlayer
    {

        [Cheat("godmode")]
        void SetHealth(float val);

        float GetCurrentHealth();

        int GetMaxHealth();

        [Cheat("infinitive shield")]
        void SetShield(float val);

        float GetCurrentShield();

        int GetMaxShield();

        [Cheat("grenades")]
        void SetGrenades(float val);

        [Cheat("badass token")]
        void GenerateBaddassToken();

        void DisableBadassToken();

        Location GetCurrentLocation();

        void TeleportJump();

        [Cheat("teleport")]
        void TeleportTo(TeleportType type);

        void UndoTeleport();

        [Cheat("level")]
        void SetLevel(int num);

        int GetMoney();

        [Cheat("money")]
        void SetMoney(int money);

        int GetEridium();

        [Cheat("erdium")]
        void SetEridium(int amount);

        int GetSeraphCrystals();

        [Cheat("seraph crystals")]
        void SetSeraphCrystals(int amount);

        int GetTorgueTokens();

        [Cheat("torgue tokens")]
        void SetTorgueTokens(int amount);

        [Cheat("fly")]
        void Fly();

    }
}
