﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands2Trainer.cheats
{
    public class TeleportType
    {
        protected static HashSet<TeleportType> types = new HashSet<TeleportType>();

        public static TeleportType WILDLIFE_EXPLOTATION_PRESERVE_MIDGETS = new TeleportType(
            "WILDLIFE_EXPLOTATION_PRESERVE -> [LEGENDARY_MIDGETS]",
            "the legendary midgets inside the wildlife explotation preserve",
            "" + -32996.91016,
            "" + -614.1502686,
            "" + 28386.99414);
        public static TeleportType LAIR_OF_INFINITE_AGONY_ANCIENT_DRAGONS = new TeleportType(
            "LAIR_OF_INFINITE_AGONY -> [ANCIENT_DRAGONS]",
            "the ancient dragons!",
            "" + 11970.97656,
            "" + 341.6071472,
            "" + 14882.14551);
        public static TeleportType LAIR_OF_INFINITE_AGONY_BADASS_WIZARD = new TeleportType(
            "LAIR_OF_INFINITE_AGONY -> [BADASS_WIZARD(Chain Lightning legendary)]",
            "the baddass wizards in the lair of infinite agony",
            "" + 16010.96289,
            "" + 4933.610352,
            "" + 14.17459965);
        public static TeleportType LAIR_OF_INFINITE_AGONY_DRAGON_KEEP_PORTAL = new TeleportType(
            "LAIR_OF_INFINITE_AGONY_DRAGON_KEEP_PORTAL -> [HANDSOME_JACK_WIZARD]",
            "the dragon keep portal!",
            "" + -22.58593941,
            "" + 4109.61377,
            "" + -27290.69531);
        public static TeleportType MT_SACRAB_RESEARCH_CENTER_CASSIUS = new TeleportType(
            "MT_SACRAB_RESEARCH_CENTER -> [Cassius boss]",
            "the cassius boss inside the MT sacrab research center",
            ""+ 1249.177612,
            ""+ 4415.777344,
            ""+ 166.3434143
            );
        public static TeleportType HELIOS_FALLEN_URANUS = new TeleportType(
            "HELIOS_FALLEN -> [Uranus]",
            "to Uranus in helios fallen",
            ""+35780.57031,
            ""+18479.92188,
            ""+-105342.4688
        );
        public static TeleportType RUSTYARDS_HEBERT = new TeleportType(
            "RUSTYARDS -> [Hebert]",
            "to Hebert his house in the rustyyards",
            "" + -173.7341309,
            "" + 3556.456543,
            "" + 23696.0293
        );
        public static TeleportType THE_RAID_ON_DIGISTRUCT_PEAK_TANIS = new TeleportType(
            "THE_RAID_ON_DIGISTRUCT_PEAK -> [Tanis]",
            "to Tanis in digistruct peak",
            ""+ 3147.4104,
            ""+ 8615.149414,
            ""+8363.71875
        );
        public static TeleportType THE_RAID_ON_DIGISTRUCT_PEAK_OP6_FINAL_ARENA = new TeleportType(
            "THE_RAID_ON_DIGISTRUCT_PEAK_OP6_FINAL_ARENA -> [final arena for op6 and below]",
            "to the final digistruct peak arena for overpowered level 6 and below",
            "" + 2332.132568,
            "" + 3883.149902,
            "" + 952.6122437
        );
        public static TeleportType THE_RAID_ON_DIGISTRUCT_PEAK_OP7_FINAL_ARENA = new TeleportType(
            "THE_RAID_ON_DIGISTRUCT_PEAK_OP7_FINAL_ARENA -> [final arena for op7 and above]",
            "to the final digistruct peak arena for overpowered level 7 and above",
            "" + 14384.59277,
            "" + 4267.966797,
            "" + 12897.51562
        );
        public static TeleportType THE_RAID_ON_DIGISTRUCT_PEAK_OP6_FINAL_END_CHEST = new TeleportType(
            "THE_RAID_ON_DIGISTRUCT_PEAK_OP6_FINAL_END_CHEST -> [final end chest for op6 and lower]",
            "to the digistruct peak final loot chest for overpowered level 6 and below",
            "" + 6985.028809,
            "" + 4209.123535,
            "" + 1705.770508
        );
        public static TeleportType THREEHORNS_DIVIDE_SAVAGE_LEE = new TeleportType(
            "THREEHORNS_DIVIDE_SAVAGE_LEE -> [teleports to the farest location for savage lee]",
            "to the farest location for savage lee in the three horns divide",
            "" + -20553.89648,
            "" + 2799.462891,
            "" + 37036.08984
        );
        public static TeleportType THE_BURROWS_HADERAX_GODMODE_CHEST_LOCATION = new TeleportType(
            "THE_BURROWS_HADERAX_GODMODE_CHEST_LOCATION -> [teleports to the rare secret chest location where you are invincible for haderax]",
            "to the haderax secret chest location in the burrows",
            ""+ 4260.902832,
            ""+ 9243.202148,
            ""+ 17434.75586
        );
        public static TeleportType THE_BURROWS_HADERAX_GODMODE_LOCATION = new TeleportType(
            "THE_BURROWS_HADERAX_GODMODE_LOCATION -> [teleports to a location where you are invincible for haderax]",
            "to the haderax location where you are invincible inside the burrows",
            "" + 3378.302002,
            "" + 5826.192383,
            "" + 4628.888184
        );
        public static TeleportType THOUSANT_CUTS_THE_BUNKER = new TeleportType(
            "THOUSANT_CUTS_THE_BUNKER -> [teleports to the bunker boss]",
            "to the bunker location in the thousant cuts",
            "" + 12659.28711,
            "" + 8855.286133,
            "" + 15970.25586
        );
        public static TeleportType HEROS_PASS_THE_WARRIOR = new TeleportType(
            "HEROS_PASS -> [teleports to the warrior]",
            "to the warrior boss in the heros pass",
            ""+ -19543.32,
            ""+ 4148,
            ""+ 31380.18
         );
        public static TeleportType WASHBURN_REFINERY_HYPERIOUS_THE_INVINCIBLE = new TeleportType(
            "WASHBURN_REFINERY -> [teleports to Hyperious The Invincible]",
            "to Hyperious The Invincible in washburn refinery",
            "" + 42026.81,
            "" + 2352.04,
            "" + 87.68
         );
        public static TeleportType IMMORTAL_WOODS_EVIL_MONK_CLASS_LOOTCHEST = new TeleportType(
        "IMMORTAL_WOODS -> [teleports to the evil monk class lootchest in the immortal woods]",
        "to the lootchest for evil monk class in the immortal woods",
        "" + 11295.92,
        "" + -7314.08,
        "" + 20812.53
 );


        private String name;
        private String voicename;
        private String x;
        private String y;
        private String z;
        protected bool isCustom = false;

        public TeleportType(String name, String voicename, String x, String y, String z, bool isCustom = false)
        {
            this.name = name;
            this.isCustom = false;
            this.voicename = voicename;
            this.x = x;
            this.y = y;
            this.z = z;

            if(this.isCustom)
            {
                foreach(TeleportType type in types)
                {
                    if(type.Equals(this))
                    {
                        types.Remove(type);
                        break;
                    }
                }
            }

            types.Add(this);
        }

        public String GetName()
        {
            return this.name;
        }

        public String getVoiceName()
        {
            return this.voicename;
        }

        public String GetX()
        {
            return this.x;
        }

        public String GetY()
        {
            return this.y;
        }

        public String GetZ()
        {
            return this.z;
        }

        public static TeleportType[] Values()
        {
            return types.ToArray();
        }

        public static TeleportType ValueOf(String name)
        {
            foreach(TeleportType type in Values())
            {
                if(type.GetName().ToLower().Equals(name.ToLower()))
                {
                    return type;
                }
            }
            throw new Exception("invalid value "+name+"!");
        }

        public override bool Equals(object obj)
        {
            if(this.GetName().Equals(((TeleportType)obj).GetName()))
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (this.GetName() + this.isCustom).GetHashCode();
        }
    }
}
