﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands2Trainer.cheats
{
    public class CustomTeleportType : TeleportType
    {

        public CustomTeleportType(String name, String voicename, String x, String y, String z, bool isCustom = true) : base(name, voicename, x, y, z, isCustom) {}

        public bool IsCustom()
        {
            return this.isCustom;
        }

        public void Remove()
        {
            types.Remove(this);
        }

    }
}
