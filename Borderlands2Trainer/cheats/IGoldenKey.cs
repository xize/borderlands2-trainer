﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands2Trainer.cheats
{
    interface IGoldenKey
    {
        
        int GetGoldenKeys();

        [Cheat("golden keys")]
        void SetGoldenKeys(int amount);

    }
}
