﻿using Borderlands2Trainer.controls;

namespace Borderlands2Trainer
{
    partial class TeleportManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TeleportManager));
            this.label1 = new System.Windows.Forms.Label();
            this.savelistbtn = new System.Windows.Forms.Button();
            this.loadlistbtn = new System.Windows.Forms.Button();
            this.backupbtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.xyzcoordlabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.addbtn = new System.Windows.Forms.Button();
            this.savedatavoicetextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.savedatanametextbox = new System.Windows.Forms.TextBox();
            this.deletebtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.teleportlist = new Borderlands2Trainer.controls.EditableListbox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(355, 2);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(25, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // savelistbtn
            // 
            this.savelistbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.savelistbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.savelistbtn.ForeColor = System.Drawing.Color.White;
            this.savelistbtn.Location = new System.Drawing.Point(184, 167);
            this.savelistbtn.Name = "savelistbtn";
            this.savelistbtn.Size = new System.Drawing.Size(62, 23);
            this.savelistbtn.TabIndex = 2;
            this.savelistbtn.Text = "save list";
            this.savelistbtn.UseVisualStyleBackColor = true;
            this.savelistbtn.Click += new System.EventHandler(this.Savelistbtn_Click);
            // 
            // loadlistbtn
            // 
            this.loadlistbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.loadlistbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadlistbtn.ForeColor = System.Drawing.Color.White;
            this.loadlistbtn.Location = new System.Drawing.Point(252, 167);
            this.loadlistbtn.Name = "loadlistbtn";
            this.loadlistbtn.Size = new System.Drawing.Size(56, 23);
            this.loadlistbtn.TabIndex = 3;
            this.loadlistbtn.Text = "load list";
            this.loadlistbtn.UseVisualStyleBackColor = true;
            this.loadlistbtn.Click += new System.EventHandler(this.Loadlistbtn_Click);
            // 
            // backupbtn
            // 
            this.backupbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backupbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backupbtn.ForeColor = System.Drawing.Color.White;
            this.backupbtn.Location = new System.Drawing.Point(317, 167);
            this.backupbtn.Name = "backupbtn";
            this.backupbtn.Size = new System.Drawing.Size(55, 23);
            this.backupbtn.TabIndex = 4;
            this.backupbtn.Text = "backup";
            this.backupbtn.UseVisualStyleBackColor = true;
            this.backupbtn.Click += new System.EventHandler(this.Button3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.xyzcoordlabel);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(157, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(215, 46);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "current location:";
            // 
            // xyzcoordlabel
            // 
            this.xyzcoordlabel.AutoSize = true;
            this.xyzcoordlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xyzcoordlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.xyzcoordlabel.Location = new System.Drawing.Point(6, 16);
            this.xyzcoordlabel.Name = "xyzcoordlabel";
            this.xyzcoordlabel.Size = new System.Drawing.Size(126, 13);
            this.xyzcoordlabel.TabIndex = 0;
            this.xyzcoordlabel.Text = "X: ??? Y: ??? Z: ???";
            this.xyzcoordlabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.addbtn);
            this.groupBox2.Controls.Add(this.savedatavoicetextbox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.savedatanametextbox);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(157, 80);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(215, 81);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "save data:";
            // 
            // addbtn
            // 
            this.addbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addbtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.addbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addbtn.ForeColor = System.Drawing.Color.White;
            this.addbtn.Location = new System.Drawing.Point(157, 16);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(55, 62);
            this.addbtn.TabIndex = 9;
            this.addbtn.Text = "add";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.Addbtn_Click);
            // 
            // savedatavoicetextbox
            // 
            this.savedatavoicetextbox.Location = new System.Drawing.Point(48, 45);
            this.savedatavoicetextbox.Name = "savedatavoicetextbox";
            this.savedatavoicetextbox.Size = new System.Drawing.Size(100, 20);
            this.savedatavoicetextbox.TabIndex = 3;
            this.savedatavoicetextbox.Text = "to the ??";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "voice:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "name:";
            // 
            // savedatanametextbox
            // 
            this.savedatanametextbox.Location = new System.Drawing.Point(48, 18);
            this.savedatanametextbox.Name = "savedatanametextbox";
            this.savedatanametextbox.Size = new System.Drawing.Size(100, 20);
            this.savedatanametextbox.TabIndex = 0;
            // 
            // deletebtn
            // 
            this.deletebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deletebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deletebtn.ForeColor = System.Drawing.Color.White;
            this.deletebtn.Location = new System.Drawing.Point(89, 167);
            this.deletebtn.Name = "deletebtn";
            this.deletebtn.Size = new System.Drawing.Size(89, 23);
            this.deletebtn.TabIndex = 8;
            this.deletebtn.Text = "delete selected";
            this.deletebtn.UseVisualStyleBackColor = true;
            this.deletebtn.Click += new System.EventHandler(this.Button4_Click);
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(12, 167);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(47, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "sync";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // teleportlist
            // 
            this.teleportlist.BackColor = System.Drawing.Color.Black;
            this.teleportlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.teleportlist.Cursor = System.Windows.Forms.Cursors.Default;
            this.teleportlist.Editable = true;
            this.teleportlist.ForeColor = System.Drawing.Color.White;
            this.teleportlist.FormattingEnabled = true;
            this.teleportlist.Items.AddRange(new object[] {
            "test"});
            this.teleportlist.Location = new System.Drawing.Point(13, 34);
            this.teleportlist.Name = "teleportlist";
            this.teleportlist.ScrollAlwaysVisible = true;
            this.teleportlist.Size = new System.Drawing.Size(138, 117);
            this.teleportlist.TabIndex = 1;
            // 
            // TeleportManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::Borderlands2Trainer.Properties.Resources.teleportmanager1;
            this.ClientSize = new System.Drawing.Size(384, 202);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.deletebtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.backupbtn);
            this.Controls.Add(this.loadlistbtn);
            this.Controls.Add(this.savelistbtn);
            this.Controls.Add(this.teleportlist);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TeleportManager";
            this.Text = "TeleportManager";
            this.Load += new System.EventHandler(this.TeleportManager_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private EditableListbox teleportlist;
        private System.Windows.Forms.Button savelistbtn;
        private System.Windows.Forms.Button loadlistbtn;
        private System.Windows.Forms.Button backupbtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox savedatanametextbox;
        private System.Windows.Forms.Button deletebtn;
        private System.Windows.Forms.TextBox savedatavoicetextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Label xyzcoordlabel;
        private System.Windows.Forms.Button button1;
    }
}