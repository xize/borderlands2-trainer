﻿using Borderlands2Trainer.util;
using gitlabupdater;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    public partial class AutoUpdateWindowConfirm : Form
    {
        private Updater updater;

        private bool isMouseClicked = false;
        private Point lastLocation;
        private Cursor ungrab;
        private Cursor grab;

        public AutoUpdateWindowConfirm()
        {
            this.ungrab = new Cursor(Properties.Resources.grabicon1.GetHicon());
            this.grab = new Cursor(Properties.Resources.grabicon2.GetHicon());
            InitializeComponent();
        }

        private void AutoUpdateWindowConfirm_Load(object sender, EventArgs e)
        {
            this.makeMoveAble(this);
            this.progress.Hide();
            this.processlabel.Hide();
            this.changelog.Hide();
            this.Height = 109;

            CornerUtil.MakeRoundedCorners(this, 20);
            foreach (Control c in this.Controls)
            {
                if(c is Button)
                {
                    CornerUtil.MakeRoundedCorners(c, 5);
                }
            }
        }

        public int Progress
        {
            set
            {
                if(!this.progress.Visible)
                {
                    this.progress.Show();
                }
                this.progress.Value = value;
            }
        }

        public Updater Updater
        {
            set
            {
                this.updater = value;
            }
        }

        public String NewVersion
        {
            get
            {
                return this.newversion.Text;
            }
            set
            {
                this.newversion.Text = value;
            }
        }

        public String OldVersion
        {
            get
            {
                return this.oldversion.Text;
            }
            set
            {
                this.oldversion.Text = value;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.processlabel.Show();
            BackgroundWorker w = new BackgroundWorker();
            w.WorkerReportsProgress = true;

            w.DoWork += new DoWorkEventHandler((object a, DoWorkEventArgs args) =>
            {
                this.updater.Download(w);

                w.ReportProgress(100);
                Thread.Sleep(1200);
            });
            w.ProgressChanged += new ProgressChangedEventHandler((object a, ProgressChangedEventArgs args) =>
            {
                if (args.ProgressPercentage == 10) {
                    this.processlabel.Text = "process: specify download location";
                } else if(args.ProgressPercentage == 40)
                {
                    this.processlabel.Text = "process: downloading file...";
                } else if(args.ProgressPercentage == 50)
                {
                    this.processlabel.Text = "process: download completed";
                } else if(args.ProgressPercentage == 60)
                {
                    this.processlabel.Text = "process: checking file checksums!";
                } else if(args.ProgressPercentage == 80)
                {
                    this.processlabel.Text = "process: checksum mathed!";
                }

                this.Progress = args.ProgressPercentage;
            });
            w.RunWorkerCompleted += new RunWorkerCompletedEventHandler((object a, RunWorkerCompletedEventArgs args) =>
            {
                this.Close();
            });
            w.RunWorkerAsync();

        }

        private void Label5_Click(object sender, EventArgs e)
        {
            this.changelog.Text = "Changes: \n\n";
            if(this.updater.Changelog != null)
            {
                foreach(String line in this.updater.Changelog)
                {
                    this.changelog.AppendText(line+"\n");
                }
            } else
            {
                this.changelog.AppendText("- no changes have been noted\n");
            }

            this.changelog.Show();
            this.changelog.Height = 51;
            this.Height = 198;
            CornerUtil.MakeRoundedCorners(this, 20);

            this.label5.Hide();
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void makeMoveAble(Control c)
        {
            c.MouseDown += new MouseEventHandler(moveableWindowHoldEvent);
            c.MouseUp += new MouseEventHandler(moveableWindowUpEvent);
            c.MouseMove += new MouseEventHandler(moveableWindowMoveEvent);

        }

        private void moveableWindowMoveEvent(object sender, MouseEventArgs e)
        {
            if (this.isMouseClicked)
            {

                this.Location = new Point((this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void moveableWindowHoldEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Cursor = this.grab;
                this.isMouseClicked = true;
                this.lastLocation = e.Location;
            }
        }

        private void moveableWindowUpEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Cursor = this.ungrab;
                this.isMouseClicked = false;
            }
        }

    }
}
