﻿using Borderlands2Trainer.cheats;
using Borderlands2Trainer.util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    partial class Window
    {

        private TeleportManager tpmanager;

        public void DoTeleport()
        {
            TeleportType type = TeleportType.ValueOf((String)this.teleportbox.SelectedItem);

            Speech.Speak(this.mute, "teleporting to " + type.getVoiceName());

            Speech.Speak(this.mute, "teleporting to " + type.getVoiceName());

            if (type == TeleportType.WILDLIFE_EXPLOTATION_PRESERVE_MIDGETS)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.WILDLIFE_EXPLOTATION_PRESERVE_MIDGETS);
            }
            else if (type == TeleportType.LAIR_OF_INFINITE_AGONY_ANCIENT_DRAGONS)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.LAIR_OF_INFINITE_AGONY_ANCIENT_DRAGONS);
            }
            else if (type == TeleportType.LAIR_OF_INFINITE_AGONY_BADASS_WIZARD)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.LAIR_OF_INFINITE_AGONY_BADASS_WIZARD);
            }
            else if (type == TeleportType.LAIR_OF_INFINITE_AGONY_DRAGON_KEEP_PORTAL)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.LAIR_OF_INFINITE_AGONY_DRAGON_KEEP_PORTAL);
            }
            else if (type == TeleportType.MT_SACRAB_RESEARCH_CENTER_CASSIUS)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.MT_SACRAB_RESEARCH_CENTER_CASSIUS);
            }
            else if (type == TeleportType.HELIOS_FALLEN_URANUS)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.HELIOS_FALLEN_URANUS);
            }
            else if (type == TeleportType.RUSTYARDS_HEBERT)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.RUSTYARDS_HEBERT);
            }
            else if (type == TeleportType.THE_RAID_ON_DIGISTRUCT_PEAK_TANIS)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.THE_RAID_ON_DIGISTRUCT_PEAK_TANIS);
            }
            else if (type == TeleportType.THE_RAID_ON_DIGISTRUCT_PEAK_OP6_FINAL_ARENA)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.THE_RAID_ON_DIGISTRUCT_PEAK_OP6_FINAL_ARENA);
            }
            else if (type == TeleportType.THE_RAID_ON_DIGISTRUCT_PEAK_OP6_FINAL_END_CHEST)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.THE_RAID_ON_DIGISTRUCT_PEAK_OP6_FINAL_END_CHEST);
            }
            else if (type == TeleportType.THE_RAID_ON_DIGISTRUCT_PEAK_OP7_FINAL_ARENA)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.THE_RAID_ON_DIGISTRUCT_PEAK_OP7_FINAL_ARENA);
            }
            else if (type == TeleportType.THREEHORNS_DIVIDE_SAVAGE_LEE)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.THREEHORNS_DIVIDE_SAVAGE_LEE);
            }
            else if (type == TeleportType.THE_BURROWS_HADERAX_GODMODE_CHEST_LOCATION)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.THE_BURROWS_HADERAX_GODMODE_CHEST_LOCATION);
            }
            else if (type == TeleportType.THE_BURROWS_HADERAX_GODMODE_LOCATION)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.THE_BURROWS_HADERAX_GODMODE_LOCATION);
            }
            else if (type == TeleportType.THOUSANT_CUTS_THE_BUNKER)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.THOUSANT_CUTS_THE_BUNKER);
            }
            else if (type == TeleportType.HEROS_PASS_THE_WARRIOR)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.HEROS_PASS_THE_WARRIOR);
            }
            else if (type == TeleportType.WASHBURN_REFINERY_HYPERIOUS_THE_INVINCIBLE)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.WASHBURN_REFINERY_HYPERIOUS_THE_INVINCIBLE);
            }
            else if (type == TeleportType.IMMORTAL_WOODS_EVIL_MONK_CLASS_LOOTCHEST)
            {
                this.trainer.GetPlayer().TeleportTo(TeleportType.IMMORTAL_WOODS_EVIL_MONK_CLASS_LOOTCHEST);
            }
            else
            {
                try
                {
                    if (type is CustomTeleportType)
                    {
                        this.trainer.GetPlayer().TeleportTo(type);
                    }
                }
                catch (Exception)
                {
                    //do nothing :)
                }
            }
        }

        public void RebuildTeleportList()
        {
            this.teleportbox.Items.Clear();
            this.teleportitem.DropDownItems.Clear();
            foreach (TeleportType tel in TeleportType.Values())
            {
                this.teleportbox.Items.Add(tel.GetName());

                ToolStripMenuItem item = new ToolStripMenuItem(tel.GetName());
                item.BackColor = Color.Black;
                item.ForeColor = Color.White;
                item.Click += new EventHandler(setlocationtpmenu);
                this.teleportitem.DropDownItems.Add(item);
            }
            this.teleportbox.SelectedIndex = 0;
        }

    }
}
