﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    public partial class ErrorWindow : Form
    {

        [DllImport("shell32.dll", EntryPoint = "ExtractIcon")]
        public extern static IntPtr ExtractIcon(IntPtr hInst, string lpszExeFileName, int nIconIndex);


        private String path = @"C:\WINDOWS\System32\shell32.dll";

        public ErrorWindow()
        {
            InitializeComponent();
        }

        public ErrorWindow(String content)
        {
                this.InitializeComponent();
                this.TextArea = content;
            this.FormClosing += new FormClosingEventHandler(closeform);
        }

        public ErrorWindow(String content, String title)
        {
                this.InitializeComponent();
                this.TextArea = content;
                this.Title = title;
            this.FormClosing += new FormClosingEventHandler(closeform);
        }

        private void closeform(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }

        [Description("the title of the window"), Category("Appearance")]
        public String Title
        {
            get
            {
                return this.Text;
            } set
            {
                this.Text = value;
            }
        }

        [Description("the text to be edited for the stacktrace"), Category("Appearance")]
        public String TextArea
        {
            get
            {
                return this.richTextBox1.Text;
            }
            set
            {
                this.richTextBox1.Text = value;
            }
        }

        private Icon GetIcon(int index)
        {
            IntPtr Hicon = ExtractIcon(
               IntPtr.Zero, path, index);
            Icon icon = Icon.FromHandle(Hicon);
            return icon;
        }

        private void ErrorWindow_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(onKeypress);
            this.errorpanel.BackgroundImage = GetIcon(77).ToBitmap();
            this.Icon = GetIcon(109);
        }

        private void onKeypress(object sender, KeyPressEventArgs e)
        {
           if(e.KeyChar == (char)Keys.Escape)
            {
                this.Close();
            }
        }

        private void Copybtn_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.TextArea);
        }
    }
}
