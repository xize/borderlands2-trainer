﻿using Borderlands2Trainer;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    class HotKeyProvider
    {

        private Form win;
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        public HotKeyProvider(Window win)
        {
            this.win = win;
            win.FormClosing += new FormClosingEventHandler(onCloseHotkey);
        }

        public void RegisterKey(Keys key)
        {
            RegisterHotKey(this.win.Handle, 0, (int)KeyModifier.None, key.GetHashCode());
        }

        public void UnRegisterAll()
        {
            UnregisterHotKey(this.win.Handle, 0);
        }

        enum KeyModifier
        {
            None = 0,
            Alt = 1,
            Control = 2,
            Shift = 4,
            WinKey = 8
        }

        private void onCloseHotkey(object sender, FormClosingEventArgs e)
        {
            UnregisterHotKey(this.win.Handle, 0);
        }

    }
}
