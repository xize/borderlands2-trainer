﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    public partial class LoadWindow : Form
    {

        [DllImport("user32.dll", EntryPoint = "HideCaret")]
        public static extern long HideCaret(IntPtr hwnd);
        private BackgroundWorker worker;
        private Random rand = new Random();
        private bool debug;
        private double progress = 0.0;

        public LoadWindow(bool debug = false)
        {
            InitializeComponent();
            this.loadingani.Image = Properties.Resources.ezgif_4_2953749d2ff8;
            this.debug = debug;
            this.Opacity = 0.0;
            this.loadprogress.Text = String.Format(this.loadprogress.Text, this.progress);
            this.LoadP();
        }

        public void LoadP()
        {
            this.worker = new BackgroundWorker();
            this.worker.DoWork += new DoWorkEventHandler(dowork);
            this.worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(workcompleted);
            this.worker.RunWorkerAsync();
        }

        private void dowork(object sender, DoWorkEventArgs e)
        {
            while(true)
            {
                bool cclose = false;

                //synchronize :)
                this.Invoke((MethodInvoker)delegate ()
               {
                   {
                       if (this.Opacity >= 1.0)
                       {
                            
                           if(progress == 0)
                           {
                               double b = this.progress;
                               this.progress = this.progress + rand.Next(0, 13) + this.rand.NextDouble();
                               if (this.progress >= 100)
                               {
                                   this.progress = 100;
                               }
                               this.loadprogress.Text = this.loadprogress.Text.Replace(Math.Round(b, 1).ToString(), Math.Round(this.progress, 1).ToString());
                           } else if(progress >= 100)
                           {
                               cclose = !cclose;
                           } else
                           {

                               double b = this.progress;
                               this.progress = this.progress + rand.Next(0, 13) + this.rand.NextDouble();
                               if (this.progress >= 100)
                               {
                                   this.progress = 100;
                               }
                               this.loadprogress.Text = this.loadprogress.Text.Replace(Math.Round(b, 1).ToString(),  Math.Round(this.progress, 1).ToString());

                           }

                       }
                       else
                       {
                           this.Opacity += 0.1;
                       }
                   }
               });

                if(cclose)
                {
                    break;
                }

              Thread.Sleep(100);
            }
        }


    private void workcompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Hide();
            Window win = new Window(this.debug);
            win.Show();
        }

        private void LoadWindow_Load(object sender, EventArgs e)
        {
            this.version.LostFocus += new EventHandler(versionfocus);
            this.version.Text = String.Format("{0}", Application.ProductVersion);

            this.version.SelectionStart = this.Text.Length - 4;
            this.version.SelectionLength = this.Text.Length;
            this.version.SelectionColor = Color.FromArgb(0, 158, 224);
            this.version.SelectionFont = new Font(FontFamily.GenericSansSerif, 16 , FontStyle.Bold);
            this.version.SelectionStart = this.version.Text.Length;
            this.version.DeselectAll();
            
        }

        private void versionfocus(object sender, EventArgs e)
        {
            HideCaret(((RichTextBox)sender).Handle);
        }
    }
}
