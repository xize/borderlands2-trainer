﻿using Borderlands2Trainer.cheats;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands2Trainer
{
    public partial class CompileDebugWindow : Form
    {

        private Window win;
        private Mem m;

        public CompileDebugWindow(Window win, Mem m)
        {
            this.win = win;
            this.m = m;
            InitializeComponent();
        }

        private void CompileDebugWindow_Load(object sender, EventArgs e)
        {
            this.savefile.RestoreDirectory = true;
            this.savefile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            locationlabel.Text = String.Format("{0}", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
        }

        private void Button1_Click(object sender, EventArgs e)
        {

            DialogResult sr = this.savefile.ShowDialog();
            
            if(sr != DialogResult.OK)
            {
                return;
            }

            locationlabel.Text = String.Format("{0}", this.savefile.FileName);

            MessageBox.Show("please wait generating debug files", "");

            DialogResult r = MessageBox.Show("is the map loaded y/n?", "debug question", MessageBoxButtons.YesNo);
            if(r == DialogResult.Yes)
            {

                String datafolder = Path.GetTempPath() + "/bltrainer";

                if(File.Exists(this.savefile.FileName))
                {
                    File.Delete(this.savefile.FileName);
                }

                ZipFile.CreateFromDirectory(datafolder, this.savefile.FileName);

                Directory.Delete(datafolder, true);

                MessageBox.Show("done!, debug file created");
            } else
            {
                MessageBox.Show("please load the trainer when the game is active and use this button again when the map is loaded", "debug info");
            }
        }
    }
}
