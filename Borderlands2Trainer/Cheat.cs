﻿using Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Borderlands2Trainer
{
    public abstract class Cheat
    {
        protected Mem m;
        protected static bool isAOB = false;
        protected bool isFrozen = false;

        public Cheat(Mem m)
        {
            this.m = m;
        }

        public abstract CheatType getCheatType();

        public abstract void RunCheat();

        public abstract void GenerateDebugData();

        public abstract void Freeze();

        public abstract void UnFreeze();

        public static void setAOB(bool bol)
        {
            isAOB = bol;
        }

    }
}
